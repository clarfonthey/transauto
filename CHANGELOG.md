This project uses a **major**.**minor**.**micro** versioning scheme, where:

* Bumping the major version resets the minor version to zero.
* Bumping the minor version resets the micro version to zero.
* The major version is bumped on breaking changes to the `transauto` crate as defined by Rust RFC 1122.
* The minor version is bumped on minor changes to the `transauto` crate, as defined by Rust RFC 1122.
* The micro version is bumped in all other cases.

For versions before 1.0.0, the project uses the scheme 0.**major**.**minor** scheme instead. This simply indicates that the API is undergoing rapid prototyping and will bump the **minor** version on any non-breaking changes.

# v0.1.0

This is the first release.
