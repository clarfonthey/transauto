# transauto

Traits for transducers and automata.

The ultimate goal is to have an abstraction that will both generalise and match the benchmarks for existing ecosystem crates like [`regex`] and [`fst`]. However, that's going to take a while, and in the meantime, the API provided should be considered incredibly volatile and prone to change at any time.

[`regex`]: https://crates.io/crates/regex
[`fst`]: https://crates.io/crates/fst

Additionally, this API *could* be improved by nightly features such as [return-position `impl Trait` in traits][RPTIT] and [refined trait implementations][refine], but currently avoids them in an effort to make the API actually work before they're implemented. The final version of the API will likely happen after these features have at least settled, and will likely take advantage of them.

[RPTIT]: https://github.com/rust-lang/rust/issues/91611
[refine]: https://github.com/rust-lang/rust/issues/100706

## License

Available via the [Anti-Capitalist Software License][ACSL] for individuals, non-profit
organisations, and worker-owned businesses.

[ACSL]: ./LICENSE.md
