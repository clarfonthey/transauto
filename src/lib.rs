//! Traits for transducers and automata.
#![cfg_attr(not(any(feature = "std", test, doc)), no_std)]
#![cfg_attr(doc, feature(doc_cfg))]
#![feature(coverage_attribute)]
#![feature(macro_metavar_expr)]
#![feature(return_type_notation)]
#![feature(trait_alias)]

#[cfg(feature = "alloc")]
extern crate alloc;

/// Prelude for this crate, to be glob-imported in relevant code.
pub mod prelude {
    #[doc(no_inline)]
    pub use crate::{
        auto::Automaton,
        //runner::{self, Runner},
        trans::Transducer,
    };
}

pub mod auto;
//pub mod runner;
pub(crate) mod hash;
pub(crate) mod iter;
pub mod scalars;
pub mod trans;

pub use crate::{
    auto::Automaton,
    //runner::Runner,
    scalars::{Index, Input, Output, State, Status},
    trans::Transducer,
};
