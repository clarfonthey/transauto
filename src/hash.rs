//! Helpers for hashing.
#[cfg(feature = "alloc")]
use core::{fmt::Debug, hash::BuildHasher as CoreHasher};

/// Nicer hash-builder for use in other types.
#[cfg(feature = "alloc")]
pub trait BuildHasher = CoreHasher + Clone + Default + Debug;
