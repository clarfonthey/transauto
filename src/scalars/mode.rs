//! [`Mode`] and [`ModeStatus`] types.
use core::mem;

use crate::scalars::Status;

/// Matching mode for an input.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Default)]
pub enum Mode {
    /// Require an exact match.
    #[default]
    Exact = 0b00,

    /// Accept any prefix.
    Prefix = 0b10,
}
impl Mode {
    /// Whether this is exact mode.
    #[inline]
    #[coverage(off)]
    pub const fn is_exact(self) -> bool {
        matches!(self, Mode::Exact)
    }

    /// Whether this is prefix mode.
    #[inline]
    #[coverage(off)]
    pub const fn is_prefix(self) -> bool {
        matches!(self, Mode::Prefix)
    }
}

/// Match mode and status for an input.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Default)]
pub enum ModeStatus {
    /// Reject exactly this sequence.
    RejectExact = 0b00,

    /// Accept exactly this sequence.
    #[default]
    AcceptExact = 0b01,

    /// Reject any sequence with this prefix.
    RejectPrefix = 0b10,

    /// Accept any sequence with this prefix.
    AcceptPrefix = 0b11,
}
impl ModeStatus {
    /// Sets from a [`Mode`] and [`Status`].
    pub const fn new(mode: Mode, status: Status) -> ModeStatus {
        // SAFETY: We only return two bits, which is valid for SeqType.
        unsafe { mem::transmute::<u8, ModeStatus>((mode as u8) | (status as u8)) }
    }

    /// Status for the sequence.
    pub const fn status(self) -> Status {
        // SAFETY: We only return a single bit, which is valid for Status.
        unsafe { mem::transmute::<u8, Status>(self as u8 & 0b01) }
    }

    /// Match mode for the sequence.
    pub const fn mode(self) -> Status {
        // SAFETY: We only return a single bit, which is valid for Status.
        unsafe { mem::transmute::<u8, Status>(self as u8 & 0b1) }
    }
}
