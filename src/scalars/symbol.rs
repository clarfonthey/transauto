//! [`Input`] and [`Output`] types.
use core::{
    convert::Infallible,
    fmt::Debug,
    hash::Hash,
    num::{
        NonZeroI8, NonZeroI16, NonZeroI32, NonZeroI64, NonZeroI128, NonZeroIsize, NonZeroU8,
        NonZeroU16, NonZeroU32, NonZeroU64, NonZeroU128, NonZeroUsize,
    },
};

/// Macro for implementing [`Input`] and [`Output`] for tuples.
macro_rules! impl_symbol {
    (
        impl $symbol:ident for tuple ();
    ) => {
        impl $symbol for () {}
    };
    (
        impl $symbol:ident for tuple ($head:ident, $($tail:ident,)*);
    ) => {
        impl_symbol! {
            impl $symbol for tuple ($($tail,)*);
        }
        impl<$head: $symbol, $($tail: $symbol),*> $symbol for ($head, $($tail),*) {}
    };
    (
        $(
            impl $symbol:ident for $($t:ty)*;
        )*
    ) => {
        $(
            $(
                impl $symbol for $t {}
            )*
        )*
    };
}

/// Input symbol for an [`Automaton`].
///
/// [`Automaton`]: crate::Automaton
pub trait Input: Output + Ord + Hash {}
impl_symbol! {
    impl Input for u8 u16 u32 u64 u128 usize;
    impl Input for i8 i16 i32 i64 i128 isize;
    impl Input for char;
    impl Input for NonZeroU8 NonZeroU16 NonZeroU32 NonZeroU64 NonZeroU128 NonZeroUsize;
    impl Input for NonZeroI8 NonZeroI16 NonZeroI32 NonZeroI64 NonZeroI128 NonZeroIsize;
}
impl<'a, S: 'a + Input> Input for &'a [S] {}
impl<S: Input> Input for Option<S> {}
impl Input for &str {}
impl Input for Infallible {}
impl_symbol! {
    impl Input for tuple (A, B, C, D, E, F, G, H, I, J, K, L,);
}

/// Output symbol for a [`Transducer`].
///
/// [`Transducer`]: crate::Transducer
pub trait Output: Copy + Debug {}
impl_symbol! {
    impl Output for u8 u16 u32 u64 u128 usize;
    impl Output for i8 i16 i32 i64 i128 isize;
    impl Output for char;
    impl Output for f32 f64;
    impl Output for NonZeroU8 NonZeroU16 NonZeroU32 NonZeroU64 NonZeroU128 NonZeroUsize;
    impl Output for NonZeroI8 NonZeroI16 NonZeroI32 NonZeroI64 NonZeroI128 NonZeroIsize;
}
impl<'a, S: 'a + Output> Output for &'a [S] {}
impl<S: Output> Output for Option<S> {}
impl Output for &str {}
impl Output for Infallible {}
impl_symbol! {
    impl Output for tuple (A, B, C, D, E, F, G, H, I, J, K, L,);
}
