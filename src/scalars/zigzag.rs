//! [`Zigzag`] trait and implementations.
use core::num::NonZero;

/// Shared implementation between [`State`] and [`Index`] for signed integers.
///
/// [`State`]: crate::scalars::State
/// [`Index`]: crate::scalars::Index
pub(super) trait Zigzag: Copy + Eq {
    /// Unsigned version of integer.
    type Unsigned;

    /// Converts to an unsigned integer, using zigzag encoding.
    fn to_unsigned(self) -> Self::Unsigned;

    /// Converts from an unsigned integer, using zigzag decoding.
    fn from_unsigned(unsigned: Self::Unsigned) -> Self;

    /// Gets the next value, per the zigzag ordering.
    fn next(self) -> Self;

    /// Gets the previous value, per the zigzag ordering.
    fn prev(self) -> Option<Self>;

    /// Gets the number of items between two values, per the zigzag ordering.
    fn diff_unsigned(self, rhs: Self) -> usize;
}

/// Implements [`Zigzag`] for an integer.
macro_rules! zigzag {
    ($($i:ty => $u:ty),*$(,)?) => {
        $(
            impl Zigzag for $i {
                type Unsigned = $u;

                #[inline]
                fn to_unsigned(self) -> $u {
                    // 0, -1, 1, -2, 2, ..., MAX, -MIN
                    ((self << 1) ^ (self >> (Self::BITS - 1))) as $u
                }

                #[inline]
                fn from_unsigned(unsigned: $u) -> $i {
                    ((unsigned >> 1) as $i) ^ -((unsigned as $i) & 1)
                }

                #[inline]
                fn next(self) -> $i {
                    match self.signum() {
                        0 => -1,
                        -1 => -self,
                        1 => -self - 1,
                        _ => unreachable!(),
                    }
                }

                #[inline]
                fn prev(self) -> Option<$i> {
                    match self.signum() {
                        0 => None,
                        -1 => Some(-self - 1),
                        1 => Some(-self),
                        _ => unreachable!(),
                    }
                }

                #[inline]
                #[coverage(off)]
                fn diff_unsigned(self, rhs: $i) -> usize {
                    (rhs.to_unsigned() - self.to_unsigned()) as usize
                }
            }
            impl Zigzag for NonZero<$i> {
                type Unsigned = NonZero<$u>;

                #[inline]
                #[coverage(off)]
                fn to_unsigned(self) -> NonZero<$u> {
                    // SAFETY: Only zero maps to zero, therefore, this cannot be zero.
                    unsafe { NonZero::new_unchecked(self.get().to_unsigned()) }
                }

                #[inline]
                #[coverage(off)]
                fn from_unsigned(unsigned: NonZero<$u>) -> NonZero<$i> {
                    // SAFETY: Only zero maps to zero, therefore, this cannot be zero.
                    unsafe { NonZero::new_unchecked(<$i>::from_unsigned(unsigned.get())) }
                }

                #[inline]
                #[coverage(off)]
                fn next(self) -> NonZero<$i> {
                    // SAFETY: Zero is before all values, so, it cannot be reached here.
                    unsafe { NonZero::new_unchecked(self.get().next()) }
                }

                #[inline]
                #[coverage(off)]
                fn prev(self) -> Option<NonZero<$i>> {
                    self.get().prev().and_then(NonZero::new)
                }

                #[inline]
                #[coverage(off)]
                fn diff_unsigned(self, rhs: NonZero<$i>) -> usize {
                    self.get().diff_unsigned(rhs.get())
                }
            }
        )*
    }
}

zigzag! {
    i8 => u8,
    i16 => u16,
    i32 => u32,
    i64 => u64,
    i128 => u128,
    isize => usize,
}
