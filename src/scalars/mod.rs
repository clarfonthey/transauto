//! [`State`], [`Index`], [`Input`], and [`Output`] traits, and some things that implement them.
mod index;
mod mode;
mod state;
mod status;
mod symbol;
mod zigzag;

use self::zigzag::Zigzag;
pub use self::{
    index::{DoubleRange, Index, QuadrupleRange, SingleRange, TripleRange, ZigzagRange},
    mode::{Mode, ModeStatus},
    state::State,
    status::{BiStatus, Status},
    symbol::{Input, Output},
};
