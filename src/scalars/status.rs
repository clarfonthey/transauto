//! [`Status`] and [`BiStatus`] types.
use core::mem;

/// Status of a state.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Default)]
pub enum Status {
    /// Reject the string.
    #[default]
    Reject = 0,

    /// Accept the string.
    Accept = 1,
}
impl Status {
    /// Status which accepts if true.
    #[inline]
    #[coverage(off)]
    pub const fn accepting(accepting: bool) -> Status {
        if accepting {
            Status::Accept
        } else {
            Status::Reject
        }
    }

    /// Status which rejects if true.
    #[inline]
    #[coverage(off)]
    pub const fn rejecting(rejecting: bool) -> Status {
        if rejecting {
            Status::Reject
        } else {
            Status::Accept
        }
    }

    /// Whether this is an accept status.
    #[inline]
    #[coverage(off)]
    pub const fn is_accept(self) -> bool {
        matches!(self, Status::Accept)
    }

    /// Whether this is an reject status.
    #[inline]
    #[coverage(off)]
    pub const fn is_reject(self) -> bool {
        matches!(self, Status::Reject)
    }
}

/// Default status for a state.
///
/// Includes both the status for the state, and the default status for missing connections.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Default)]
pub enum BiStatus {
    /// Reject the string if we stay, reject the string if we leave.
    #[default]
    RejectAll = 0b00,

    /// Reject the string if we stay, accept the string if we leave.
    RejectEmpty = 0b01,

    /// Accept the string if we stay, reject the string if we leave.
    AcceptEmpty = 0b10,

    /// Accept the string if we stay, accept the string if we leave.
    AcceptAll = 0b11,
}
impl BiStatus {
    /// Sets from a pair of statuses, representing [`stay`] and [`leave`] statuses.
    ///
    /// [`stay`]: Self::stay
    /// [`leave`]: Self::leave
    pub const fn staying_leaving(stay: Status, leave: Status) -> BiStatus {
        // SAFETY: We only return two bits, which is valid for BiStatus.
        unsafe { mem::transmute::<u8, BiStatus>(((stay as u8) << 1) | (leave as u8)) }
    }

    /// Sets from a pair of statuses, representing [`leave`] and [`stay`] statuses.
    ///
    /// [`leave`]: Self::leave
    /// [`stay`]: Self::stay
    #[coverage(off)]
    pub const fn leaving_staying(leave: Status, stay: Status) -> BiStatus {
        BiStatus::staying_leaving(leave, stay)
    }

    /// Status if we stay on this state; status of empty string.
    pub const fn stay(self) -> Status {
        // SAFETY: We only return a single bit, which is valid for Status.
        unsafe { mem::transmute::<u8, Status>((self as u8 & 0b10) >> 1) }
    }

    /// Status if we leave this state; default status of nonempty strings.
    pub const fn leave(self) -> Status {
        // SAFETY: We only return a single bit, which is valid for Status.
        unsafe { mem::transmute::<u8, Status>(self as u8 & 0b1) }
    }
}
