//! [`State`] type.
use core::num::NonZero;

use crate::scalars::{Input, Zigzag};

/// State for an [`Automaton`].
///
/// [`Automaton`]: crate::Automaton
pub trait State: Input {
    /// Starting state.
    const STARTING: Self;

    /// Maximum possible state, i.e., where [`bump`](Self::bump) would panic.
    const MAX: Self;

    /// Gets the next state after a given state.
    ///
    /// # Panics
    ///
    /// Panics if there are no more states.
    fn bump(self) -> Self;
}

/// Macro for implementing [`State`] for tuples.
macro_rules! impl_state {
    (
        impl State for tuple ();
    ) => {
        impl State for () {
            const STARTING: () = ();
            const MAX: () = ();
            #[coverage(off)]
            fn bump(self) {
                panic!("no more states")
            }
        }
    };
    (
        impl State for tuple ($head:ident, $($tail:ident,)*);
    ) => {
        impl_state! { impl State for tuple ($($tail,)*); }
        impl<$head: State, $($tail: State),*> State for ($head, $($tail,)*) {
            const STARTING: ($head, $($tail,)*) = (<$head>::STARTING, $(<$tail>::STARTING,)*);
            const MAX: ($head, $($tail,)*) = (<$head>::MAX, $(<$tail>::MAX,)*);
            #[coverage(off)]
            fn bump(self) -> ($head, $($tail,)*) {
                #![allow(non_snake_case)]
                let ($head, $($tail,)*) = self;
                ($head.bump(), $($tail.bump(),)*)
            }
        }
    };
    (
        $(
            impl unsigned State for $($u:ty)*;
        )*
        $(
            impl signed State from ($min:expr) for $($i:ty)*;
        )*
    ) => {
        $(
            $(
                impl State for $u {
                    const STARTING: $u = <$u>::MIN;
                    const MAX: $u = <$u>::MAX;
                    #[coverage(off)]
                    fn bump(self) -> $u {
                        self.checked_add(1).unwrap_or_else(|| panic!("no more states"))
                    }
                }
            )*
        )*
        $(
            $(
                impl State for $i {
                    const STARTING: $i = $min;
                    const MAX: $i = <$i>::MIN;
                    #[coverage(off)]
                    fn bump(self) -> $i {
                        Self::from_unsigned(self.to_unsigned().checked_add(1).unwrap_or_else(
                            || panic!("no more states"))
                        )
                    }
                }
            )*
        )*
    };
}
impl_state! {
    impl unsigned State for u8 u16 u32 u64 u128 usize;
    impl unsigned State for NonZero<u8> NonZero<u16> NonZero<u32> NonZero<u64> NonZero<u128> NonZero<usize>;
    impl signed State from (0) for i8 i16 i32 i64 i128 isize;
    impl signed State from (match NonZero::new(1) {
        Some(x) => x,
        None => unreachable!(),
    }) for NonZero<i8> NonZero<i16> NonZero<i32> NonZero<i64> NonZero<i128> NonZero<isize>;
}
impl_state! {
    impl State for tuple (A, B, C, D, E, F, G, H, I, J, K, L,);
}
impl State for char {
    const STARTING: char = char::MIN;
    const MAX: char = char::MAX;

    #[inline]
    #[coverage(off)]
    fn bump(self) -> char {
        if self == char::MAX {
            panic!("no more states");
        }
        (self..char::MAX).nth(1).unwrap()
    }
}

/// Slices are left alone when bumping.
impl<'a, S: State, I: Input> State for (S, &'a [I]) {
    const STARTING: (S, &'a [I]) = (S::STARTING, &[]);
    const MAX: (S, &'a [I]) = (S::MAX, &[]);

    #[inline]
    #[coverage(off)]
    fn bump(self) -> (S, &'a [I]) {
        (self.0.bump(), self.1)
    }
}
