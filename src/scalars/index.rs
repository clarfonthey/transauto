//! [`Index`] trait.
use core::{
    cmp::Ordering,
    convert::Infallible,
    fmt,
    hash::{Hash, Hasher},
    iter::{self, FusedIterator, Iterator as BaseIterator},
    mem,
    num::NonZero,
    ops::Range,
};

use crate::{
    iter::Iterator,
    scalars::{Input, Output, State, Zigzag},
};

/// Something which can easily be cast to an index in an array.
pub trait Index: State {
    /// Upper bound on index.
    const MAX_INDEX: usize;

    /// Non-zero version of index.
    type NonZero: Input;

    /// Iterator type for index.
    type Iter: Default + Clone + fmt::Debug + Iterator<Item = Self>;

    /// Converts to an index.
    fn to_index(self) -> usize;

    /// Converts from an index.
    ///
    /// # Panics
    ///
    /// May panic if out of bounds, but also may return invalid results.
    fn from_index(idx: usize) -> Self;

    /// Iterates over all indices from the start to the given index.
    fn upto(self) -> Self::Iter;

    /// Iterates over all indices from the start to the given index.
    fn then_upto(self, limit: Self) -> Self::Iter;

    /// Creates non-zero version of index.
    fn to_non_zero(self) -> Option<Self::NonZero>;

    /// Creates non-zero version of index without checking for zero.
    ///
    /// # Safety
    ///
    /// The caller must ensure that [`to_non_zero`](Self::to_non_zero) for `self` is `Some`.
    #[coverage(off)]
    unsafe fn to_non_zero_unchecked(self) -> Self::NonZero {
        // SAFETY: Upheld by caller.
        unsafe { self.to_non_zero().unwrap_unchecked() }
    }

    /// Converts non-zero version of index into a zeroable version.
    fn from_non_zero(nz: Self::NonZero) -> Self;
}

/// Range iterator for signed indices.
#[derive(Clone)]
pub struct ZigzagRange<T> {
    /// Start of range.
    start: T,

    /// End of range.
    end: T,
}
impl<T: Index> Default for ZigzagRange<T> {
    fn default() -> ZigzagRange<T> {
        ZigzagRange {
            start: T::STARTING,
            end: T::STARTING,
        }
    }
}
impl<T: Zigzag> BaseIterator for ZigzagRange<T> {
    type Item = T;

    #[inline]
    fn next(&mut self) -> Option<T> {
        if self.start == self.end {
            None
        } else {
            let ret = self.start;
            self.start = self.start.next();
            Some(ret)
        }
    }

    #[inline]
    #[coverage(off)]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.end.diff_unsigned(self.start);
        (len, Some(len))
    }
}
impl<T: fmt::Debug> fmt::Debug for ZigzagRange<T> {
    #[coverage(off)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&(&self.start..&self.end), f)
    }
}
impl<T: Zigzag> DoubleEndedIterator for ZigzagRange<T> {
    #[inline]
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.start == self.end {
            None
        } else {
            self.end = self.end.prev()?;
            Some(self.end)
        }
    }
}
impl<T: Zigzag> ExactSizeIterator for ZigzagRange<T> {
    #[inline]
    #[coverage(off)]
    fn len(&self) -> usize {
        self.end.diff_unsigned(self.start)
    }
}
impl<T: Zigzag> FusedIterator for ZigzagRange<T> {}

/// Range iterator for single tuples.
pub struct SingleRange<T: Index> {
    /// Inner iterator.
    one: T::Iter,
}
impl<T: Index> Clone for SingleRange<T> {
    #[inline]
    #[coverage(off)]
    fn clone(&self) -> SingleRange<T> {
        SingleRange {
            one: self.one.clone(),
        }
    }
}
impl<T: Index> Default for SingleRange<T> {
    #[inline]
    #[coverage(off)]
    fn default() -> SingleRange<T> {
        SingleRange {
            one: Default::default(),
        }
    }
}
impl<T: Index> fmt::Debug for SingleRange<T> {
    #[coverage(off)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SingleRange")
            .field("one", &self.one)
            .finish()
    }
}
impl<T: Index> SingleRange<T> {
    /// Creates a new [`SingleRange`] from its component iterator.
    #[coverage(off)]
    fn new(one: T::Iter) -> SingleRange<T> {
        SingleRange { one }
    }
}
impl<T: Index> BaseIterator for SingleRange<T> {
    type Item = (T,);

    #[inline]
    #[coverage(off)]
    fn next(&mut self) -> Option<(T,)> {
        self.one.next().map(|x| (x,))
    }

    #[inline]
    #[coverage(off)]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.one.size_hint()
    }
}
impl<T: Index> DoubleEndedIterator for SingleRange<T> {
    #[inline]
    #[coverage(off)]
    fn next_back(&mut self) -> Option<(T,)> {
        self.one.next_back().map(|x| (x,))
    }
}
impl<T: Index> ExactSizeIterator for SingleRange<T> {
    #[inline]
    #[coverage(off)]
    fn len(&self) -> usize {
        self.one.len()
    }
}
impl<T: Index> FusedIterator for SingleRange<T> {}

/// Range iterator for double tuples.
pub struct DoubleRange<T: Index, U: Index> {
    /// Current super-item.
    one_item: T,

    /// Current sub-iterator.
    two: U::Iter,

    /// Current super-item for backward direction.
    one_item_back: T,

    /// Current sub-iterator for backward direction.
    two_back: U::Iter,

    /// Iterator of super-items.
    one: T::Iter,

    /// Iterator of sub-items, to be yielded for new super-items.
    two_full: U::Iter,
}
impl<T: Index, U: Index> DoubleRange<T, U> {
    /// Creates a new [`DoubleRange`] from its component iterators.
    fn new(one: T::Iter, two: U::Iter) -> DoubleRange<T, U> {
        DoubleRange::new_partial(one, two.clone(), two)
    }

    /// Creates a new [`DoubleRange`] from its component iterators, allowing separate full and
    /// non-full iterators for the second range.
    fn new_partial(mut one: T::Iter, two: U::Iter, two_full: U::Iter) -> DoubleRange<T, U> {
        let (one_item, two) = one
            .next()
            .map_or((T::STARTING, U::Iter::default()), |x| (x, two));
        let (one_item_back, two_back) = one
            .next_back()
            .map_or((T::STARTING, U::Iter::default()), |x| (x, two_full.clone()));
        DoubleRange {
            one_item,
            two,
            one_item_back,
            two_back,
            one,
            two_full,
        }
    }
}
impl<T: Index, U: Index> Clone for DoubleRange<T, U> {
    #[inline]
    fn clone(&self) -> DoubleRange<T, U> {
        DoubleRange {
            one_item: self.one_item,
            two: self.two.clone(),
            one_item_back: self.one_item_back,
            two_back: self.two_back.clone(),
            one: self.one.clone(),
            two_full: self.two_full.clone(),
        }
    }
}
impl<T: Index, U: Index> Default for DoubleRange<T, U> {
    #[inline]
    fn default() -> DoubleRange<T, U> {
        DoubleRange {
            one_item: T::STARTING,
            two: Default::default(),
            one_item_back: T::STARTING,
            two_back: Default::default(),
            one: Default::default(),
            two_full: Default::default(),
        }
    }
}
impl<T: Index, U: Index> fmt::Debug for DoubleRange<T, U> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("DoubleRange")
            .field("next", &(&self.one_item, &self.two))
            .field("pending", &(&self.one, &self.two_full))
            .field("next_back", &(&self.one_item_back, &self.two_back))
            .finish()
    }
}
impl<T: Index, U: Index> BaseIterator for DoubleRange<T, U> {
    type Item = (T, U);

    #[inline]
    fn next(&mut self) -> Option<(T, U)> {
        loop {
            if let Some(two_item) = self.two.next() {
                return Some((self.one_item, two_item));
            }
            (self.one_item, self.two) = match self.one.next() {
                Some(one_item) => (one_item, self.two_full.clone()),
                None => {
                    self.two_back.clone().next()?;
                    (self.one_item_back, mem::take(&mut self.two_back))
                }
            }
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let (one_lo, one_hi) = self.one.size_hint();
        let (two_full_lo, two_full_hi) = self.two_full.size_hint();
        let (two_lo, two_hi) = self.two.size_hint();
        let (two_back_lo, two_back_hi) = self.two_back.size_hint();
        let lo = one_lo * two_full_lo + two_lo + two_back_lo;
        let hi = match (one_hi, two_full_hi, two_hi, two_back_hi) {
            (Some(one_hi), Some(two_full_hi), Some(two_hi), Some(two_back_hi)) => {
                Some(one_hi * two_full_hi + two_hi + two_back_hi)
            }
            _ => None,
        };
        (lo, hi)
    }
}
impl<T: Index, U: Index> DoubleEndedIterator for DoubleRange<T, U> {
    #[inline]
    fn next_back(&mut self) -> Option<(T, U)> {
        loop {
            if let Some(two_item) = self.two_back.next_back() {
                return Some((self.one_item_back, two_item));
            }
            (self.one_item_back, self.two_back) = match self.one.next_back() {
                Some(one_item_back) => (one_item_back, self.two_full.clone()),
                None => {
                    self.two.clone().next()?;
                    (self.one_item, mem::take(&mut self.two))
                }
            }
        }
    }
}
impl<T: Index, U: Index> ExactSizeIterator for DoubleRange<T, U> {
    #[inline]
    fn len(&self) -> usize {
        self.one.len() * self.two_full.len() + self.two.len() + self.two_back.len()
    }
}
impl<T: Index, U: Index> FusedIterator for DoubleRange<T, U> {}

/// Range iterator for triple tuples.
pub struct TripleRange<T: Index, U: Index, V: Index> {
    /// Inner iterator.
    inner: DoubleRange<(T, U), V>,
}
impl<T: Index, U: Index, V: Index> TripleRange<T, U, V> {
    /// Creates a new [`TripleRange`] from its component iterators.
    fn new(one: T::Iter, two: U::Iter, three: V::Iter) -> TripleRange<T, U, V> {
        TripleRange {
            inner: DoubleRange::new(DoubleRange::new(one, two), three),
        }
    }

    /// Creates a new [`TripleRange`] from its component iterators, allowing separate full and
    /// non-full iterators for the second and third ranges.
    fn new_partial(
        one: T::Iter,
        two: U::Iter,
        three: V::Iter,
        two_full: U::Iter,
        three_full: V::Iter,
    ) -> TripleRange<T, U, V> {
        TripleRange {
            inner: DoubleRange::new_partial(
                DoubleRange::new_partial(one, two, two_full),
                three,
                three_full,
            ),
        }
    }
}
impl<T: Index, U: Index, V: Index> Clone for TripleRange<T, U, V> {
    #[inline]
    fn clone(&self) -> TripleRange<T, U, V> {
        TripleRange {
            inner: self.inner.clone(),
        }
    }
}
impl<T: Index, U: Index, V: Index> Default for TripleRange<T, U, V> {
    #[inline]
    fn default() -> TripleRange<T, U, V> {
        TripleRange {
            inner: Default::default(),
        }
    }
}
impl<T: Index, U: Index, V: Index> fmt::Debug for TripleRange<T, U, V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TripleRange")
            .field("next", &(&self.inner.one_item, &self.inner.two))
            .field("pending", &(&self.inner.one, &self.inner.two_full))
            .field(
                "next_back",
                &(&self.inner.one_item_back, &self.inner.two_back),
            )
            .finish()
    }
}
impl<T: Index, U: Index, V: Index> BaseIterator for TripleRange<T, U, V> {
    type Item = (T, U, V);

    #[inline]
    fn next(&mut self) -> Option<(T, U, V)> {
        self.inner.next().map(|((x, y), z)| (x, y, z))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}
impl<T: Index, U: Index, V: Index> DoubleEndedIterator for TripleRange<T, U, V> {
    #[inline]
    fn next_back(&mut self) -> Option<(T, U, V)> {
        self.inner.next_back().map(|((x, y), z)| (x, y, z))
    }
}
impl<T: Index, U: Index, V: Index> ExactSizeIterator for TripleRange<T, U, V> {
    #[inline]
    fn len(&self) -> usize {
        self.inner.len()
    }
}
impl<T: Index, U: Index, V: Index> FusedIterator for TripleRange<T, U, V> {}

/// Range iterator for quadruple tuples.
pub struct QuadrupleRange<T: Index, U: Index, V: Index, W: Index> {
    /// Inner iterator.
    inner: DoubleRange<(T, U, V), W>,
}
impl<T: Index, U: Index, V: Index, W: Index> QuadrupleRange<T, U, V, W> {
    /// Creates a new [`QuadrupleRange`] from its component iterators.
    fn new(
        one: T::Iter,
        two: U::Iter,
        three: V::Iter,
        four: W::Iter,
    ) -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange {
            inner: DoubleRange::new(TripleRange::new(one, two, three), four),
        }
    }

    /// Creates a new [`QuadrupleRange`] from its component iterators, allowing separate full and
    /// non-full iterators for the second and third ranges.
    fn new_partial(
        one: T::Iter,
        two: U::Iter,
        three: V::Iter,
        four: W::Iter,
        two_full: U::Iter,
        three_full: V::Iter,
        four_full: W::Iter,
    ) -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange {
            inner: DoubleRange::new_partial(
                TripleRange::new_partial(one, two, three, two_full, three_full),
                four,
                four_full,
            ),
        }
    }
}
impl<T: Index, U: Index, V: Index, W: Index> Clone for QuadrupleRange<T, U, V, W> {
    #[inline]
    fn clone(&self) -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange {
            inner: self.inner.clone(),
        }
    }
}
impl<T: Index, U: Index, V: Index, W: Index> Default for QuadrupleRange<T, U, V, W> {
    #[inline]
    fn default() -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange {
            inner: Default::default(),
        }
    }
}
impl<T: Index, U: Index, V: Index, W: Index> fmt::Debug for QuadrupleRange<T, U, V, W> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("QuadrupleRange")
            .field("next", &(&self.inner.one_item, &self.inner.two))
            .field("pending", &(&self.inner.one, &self.inner.two_full))
            .field(
                "next_back",
                &(&self.inner.one_item_back, &self.inner.two_back),
            )
            .finish()
    }
}
impl<T: Index, U: Index, V: Index, W: Index> BaseIterator for QuadrupleRange<T, U, V, W> {
    type Item = (T, U, V, W);

    #[inline]
    fn next(&mut self) -> Option<(T, U, V, W)> {
        self.inner.next().map(|((x, y, z), w)| (x, y, z, w))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}
impl<T: Index, U: Index, V: Index, W: Index> DoubleEndedIterator for QuadrupleRange<T, U, V, W> {
    #[inline]
    fn next_back(&mut self) -> Option<(T, U, V, W)> {
        self.inner.next_back().map(|((x, y, z), w)| (x, y, z, w))
    }
}
impl<T: Index, U: Index, V: Index, W: Index> ExactSizeIterator for QuadrupleRange<T, U, V, W> {
    #[inline]
    fn len(&self) -> usize {
        self.inner.len()
    }
}
impl<T: Index, U: Index, V: Index, W: Index> FusedIterator for QuadrupleRange<T, U, V, W> {}

/// A non-zero pair, where at least one value is non-zero.
#[derive(Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct NonZeroPair<T: Index, U: Index>(NonZeroPairImpl<T, U>);
impl<T: Index, U: Index> Output for NonZeroPair<T, U> {}
impl<T: Index, U: Index> Input for NonZeroPair<T, U> {}
impl<T: Index, U: Index> fmt::Debug for NonZeroPair<T, U> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&<(T, U)>::from_non_zero(*self), f)
    }
}

#[allow(clippy::missing_docs_in_private_items)]
#[derive(Copy, Clone)]
enum NonZeroPairImpl<T: Index, U: Index> {
    One(T::NonZero, U),
    Two(T, U::NonZero),
    Three(T::NonZero, U::NonZero),
}
impl<T: Index, U: Index> PartialEq for NonZeroPairImpl<T, U> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        <(T, U)>::from_non_zero(NonZeroPair(*self)) == <(T, U)>::from_non_zero(NonZeroPair(*other))
    }
}
impl<T: Index, U: Index> Eq for NonZeroPairImpl<T, U> {}
impl<T: Index, U: Index> Hash for NonZeroPairImpl<T, U> {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        <(T, U)>::from_non_zero(NonZeroPair(*self)).hash(state)
    }
}
impl<T: Index, U: Index> PartialOrd for NonZeroPairImpl<T, U> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<T: Index, U: Index> Ord for NonZeroPairImpl<T, U> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        <(T, U)>::from_non_zero(NonZeroPair(*self))
            .cmp(&<(T, U)>::from_non_zero(NonZeroPair(*other)))
    }
}

/// A non-zero triple, where at least one value is non-zero.
#[derive(Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct NonZeroTriple<T: Index, U: Index, V: Index>(NonZeroTripleImpl<T, U, V>);
impl<T: Index, U: Index, V: Index> Output for NonZeroTriple<T, U, V> {}
impl<T: Index, U: Index, V: Index> Input for NonZeroTriple<T, U, V> {}
impl<T: Index, U: Index, V: Index> fmt::Debug for NonZeroTriple<T, U, V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&<(T, U, V)>::from_non_zero(*self), f)
    }
}

#[allow(clippy::missing_docs_in_private_items)]
#[derive(Copy, Clone)]
enum NonZeroTripleImpl<T: Index, U: Index, V: Index> {
    One(T::NonZero, U, V),
    Two(T, U::NonZero, V),
    Three(T::NonZero, U::NonZero, V),
    Four(T, U, V::NonZero),
    Five(T::NonZero, U, V::NonZero),
    Six(T, U::NonZero, V::NonZero),
    Seven(T::NonZero, U::NonZero, V::NonZero),
}
impl<T: Index, U: Index, V: Index> PartialEq for NonZeroTripleImpl<T, U, V> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        <(T, U, V)>::from_non_zero(NonZeroTriple(*self))
            == <(T, U, V)>::from_non_zero(NonZeroTriple(*other))
    }
}
impl<T: Index, U: Index, V: Index> Eq for NonZeroTripleImpl<T, U, V> {}
impl<T: Index, U: Index, V: Index> Hash for NonZeroTripleImpl<T, U, V> {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        <(T, U, V)>::from_non_zero(NonZeroTriple(*self)).hash(state)
    }
}
impl<T: Index, U: Index, V: Index> PartialOrd for NonZeroTripleImpl<T, U, V> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<T: Index, U: Index, V: Index> Ord for NonZeroTripleImpl<T, U, V> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        <(T, U, V)>::from_non_zero(NonZeroTriple(*self))
            .cmp(&<(T, U, V)>::from_non_zero(NonZeroTriple(*other)))
    }
}

/// A non-zero triple, where at least one value is non-zero.
#[derive(Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct NonZeroQuadruple<T: Index, U: Index, V: Index, W: Index>(
    NonZeroQuadrupleImpl<T, U, V, W>,
);
impl<T: Index, U: Index, V: Index, W: Index> Output for NonZeroQuadruple<T, U, V, W> {}
impl<T: Index, U: Index, V: Index, W: Index> Input for NonZeroQuadruple<T, U, V, W> {}
impl<T: Index, U: Index, V: Index, W: Index> fmt::Debug for NonZeroQuadruple<T, U, V, W> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&<(T, U, V, W)>::from_non_zero(*self), f)
    }
}

#[allow(clippy::missing_docs_in_private_items)]
#[derive(Copy, Clone)]
enum NonZeroQuadrupleImpl<T: Index, U: Index, V: Index, W: Index> {
    One(T::NonZero, U, V, W),
    Two(T, U::NonZero, V, W),
    Three(T::NonZero, U::NonZero, V, W),
    Four(T, U, V::NonZero, W),
    Five(T::NonZero, U, V::NonZero, W),
    Six(T, U::NonZero, V::NonZero, W),
    Seven(T::NonZero, U::NonZero, V::NonZero, W),
    Eight(T, U, V, W::NonZero),
    Nine(T::NonZero, U, V, W::NonZero),
    Ten(T, U::NonZero, V, W::NonZero),
    Eleven(T::NonZero, U::NonZero, V, W::NonZero),
    Twelve(T, U, V::NonZero, W::NonZero),
    Thirteen(T::NonZero, U, V::NonZero, W::NonZero),
    Fourteen(T, U::NonZero, V::NonZero, W::NonZero),
    Fifteen(T::NonZero, U::NonZero, V::NonZero, W::NonZero),
}
impl<T: Index, U: Index, V: Index, W: Index> PartialEq for NonZeroQuadrupleImpl<T, U, V, W> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        <(T, U, V, W)>::from_non_zero(NonZeroQuadruple(*self))
            == <(T, U, V, W)>::from_non_zero(NonZeroQuadruple(*other))
    }
}
impl<T: Index, U: Index, V: Index, W: Index> Eq for NonZeroQuadrupleImpl<T, U, V, W> {}
impl<T: Index, U: Index, V: Index, W: Index> Hash for NonZeroQuadrupleImpl<T, U, V, W> {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        <(T, U, V, W)>::from_non_zero(NonZeroQuadruple(*self)).hash(state)
    }
}
impl<T: Index, U: Index, V: Index, W: Index> PartialOrd for NonZeroQuadrupleImpl<T, U, V, W> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<T: Index, U: Index, V: Index, W: Index> Ord for NonZeroQuadrupleImpl<T, U, V, W> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        <(T, U, V, W)>::from_non_zero(NonZeroQuadruple(*self))
            .cmp(&<(T, U, V, W)>::from_non_zero(NonZeroQuadruple(*other)))
    }
}

/// Implementations of [`Index`] for primitives.
macro_rules! index_impl {
    (
        impl Index for $($u:ty)* as usize;
        impl Index for $($i:ty)* as isize;
    ) => {
        $(
            impl Index for $u {
                const MAX_INDEX: usize = <$u>::MAX as usize;

                type NonZero = NonZero<$u>;
                type Iter = Range<$u>;

                #[inline]
                fn to_index(self) -> usize {
                    self as usize
                }

                #[inline]
                fn from_index(idx: usize) -> $u {
                    idx as $u
                }

                #[inline]
                fn upto(self) -> Range<$u> {
                    0..self
                }

                #[inline]
                fn then_upto(self, limit: $u) -> Range<$u> {
                    self..limit
                }

                #[inline]
                fn to_non_zero(self) -> Option<NonZero<$u>> {
                    NonZero::new(self)
                }

                #[inline]
                unsafe fn to_non_zero_unchecked(self) -> NonZero<$u> {
                    // SAFETY: Upheld by caller.
                    unsafe { NonZero::new_unchecked(self) }
                }

                #[inline]
                fn from_non_zero(nz: NonZero<$u>) -> $u {
                    nz.get()
                }
            }
        )*
        $(
            impl Index for $i {
                const MAX_INDEX: usize = (<$i>::MIN.unsigned_abs() as usize - 1) * 2 + 1;

                type NonZero = NonZero<$i>;
                type Iter = ZigzagRange<$i>;

                #[inline]
                fn to_index(self) -> usize {
                    self.to_unsigned() as usize
                }

                #[inline]
                fn from_index(idx: usize) -> $i {
                    Self::from_unsigned(idx as $u)
                }

                #[inline]
                fn upto(self) -> ZigzagRange<$i> {
                    ZigzagRange {
                        start: 0,
                        end: self,
                    }
                }

                #[inline]
                fn then_upto(self, limit: $i) -> ZigzagRange<$i> {
                    ZigzagRange {
                        start: self,
                        end: limit,
                    }
                }

                #[inline]
                fn to_non_zero(self) -> Option<NonZero<$i>> {
                    NonZero::new(self)
                }

                #[inline]
                unsafe fn to_non_zero_unchecked(self) -> NonZero<$i> {
                    // SAFETY: Upheld by caller.
                    unsafe { NonZero::new_unchecked(self) }
                }

                #[inline]
                fn from_non_zero(nz: NonZero<$i>) -> $i {
                    nz.get()
                }
            }
        )*
    };
}
index_impl! {
    impl Index for u8 u16 usize as usize;
    impl Index for i8 i16 isize as isize;
}
impl Index for () {
    const MAX_INDEX: usize = 0;

    type NonZero = Infallible;
    type Iter = iter::Empty<()>;

    #[inline]
    fn to_index(self) -> usize {
        0
    }

    #[inline]
    fn from_index(_: usize) {}

    #[inline]
    fn upto(self) -> iter::Empty<()> {
        iter::empty()
    }

    #[inline]
    fn then_upto(self, _: ()) -> iter::Empty<()> {
        iter::empty()
    }

    #[inline]
    fn to_non_zero(self) -> Option<Infallible> {
        None
    }

    #[inline]
    unsafe fn to_non_zero_unchecked(self) -> Infallible {
        panic!("() is always a zero index")
    }

    #[inline]
    fn from_non_zero(nz: Infallible) {
        match nz {}
    }
}
impl<T: Index> Index for (T,) {
    const MAX_INDEX: usize = T::MAX_INDEX;

    type NonZero = (T::NonZero,);
    type Iter = SingleRange<T>;

    #[inline]
    fn to_index(self) -> usize {
        self.0.to_index()
    }

    #[inline]
    fn from_index(idx: usize) -> (T,) {
        (T::from_index(idx),)
    }

    #[inline]
    fn upto(self) -> SingleRange<T> {
        SingleRange::new(self.0.upto())
    }

    #[inline]
    fn then_upto(self, limit: (T,)) -> SingleRange<T> {
        SingleRange::new(self.0.then_upto(limit.0))
    }

    #[inline]
    fn to_non_zero(self) -> Option<Self::NonZero> {
        self.0.to_non_zero().map(|x| (x,))
    }

    #[inline]
    unsafe fn to_non_zero_unchecked(self) -> Self::NonZero {
        // SAFETY: Upheld by caller.
        unsafe { (self.0.to_non_zero_unchecked(),) }
    }

    #[inline]
    fn from_non_zero(nz: Self::NonZero) -> Self {
        (T::from_non_zero(nz.0),)
    }
}
impl<T: Index, U: Index> Index for (T, U) {
    const MAX_INDEX: usize = (T::MAX_INDEX + 1) * U::MAX_INDEX + (U::MAX_INDEX - 1);

    type NonZero = NonZeroPair<T, U>;
    type Iter = DoubleRange<T, U>;

    #[inline]
    fn to_index(self) -> usize {
        self.0.to_index() * U::MAX_INDEX + self.1.to_index()
    }

    #[inline]
    fn from_index(idx: usize) -> (T, U) {
        let lo = idx / U::MAX_INDEX;
        let hi = idx % U::MAX_INDEX;
        (T::from_index(lo), U::from_index(hi))
    }

    #[inline]
    fn upto(self) -> DoubleRange<T, U> {
        DoubleRange::new(self.0.upto(), self.1.upto())
    }

    #[inline]
    fn then_upto(self, limit: (T, U)) -> DoubleRange<T, U> {
        DoubleRange::new_partial(
            self.0.then_upto(limit.0),
            self.1.then_upto(limit.1),
            limit.1.upto(),
        )
    }

    #[inline]
    fn to_non_zero(self) -> Option<Self::NonZero> {
        match (self.0.to_non_zero(), self.1.to_non_zero()) {
            (None, None) => None,
            (Some(lo), None) => Some(NonZeroPair(NonZeroPairImpl::One(lo, self.1))),
            (None, Some(hi)) => Some(NonZeroPair(NonZeroPairImpl::Two(self.0, hi))),
            (Some(lo), Some(hi)) => Some(NonZeroPair(NonZeroPairImpl::Three(lo, hi))),
        }
    }

    #[inline]
    fn from_non_zero(nz: Self::NonZero) -> Self {
        match nz.0 {
            NonZeroPairImpl::One(lo, hi) => (T::from_non_zero(lo), hi),
            NonZeroPairImpl::Two(lo, hi) => (lo, U::from_non_zero(hi)),
            NonZeroPairImpl::Three(lo, hi) => (T::from_non_zero(lo), U::from_non_zero(hi)),
        }
    }
}
impl<T: Index, U: Index, V: Index> Index for (T, U, V) {
    const MAX_INDEX: usize = <((T, U), V) as Index>::MAX_INDEX;

    type NonZero = NonZeroTriple<T, U, V>;
    type Iter = TripleRange<T, U, V>;

    #[inline]
    fn to_index(self) -> usize {
        ((self.0, self.1), self.2).to_index()
    }

    #[inline]
    fn from_index(idx: usize) -> (T, U, V) {
        let ((lo, md), hi) = <((T, U), V)>::from_index(idx);
        (lo, md, hi)
    }

    #[inline]
    fn upto(self) -> TripleRange<T, U, V> {
        TripleRange::new(self.0.upto(), self.1.upto(), self.2.upto())
    }

    #[inline]
    fn then_upto(self, limit: (T, U, V)) -> TripleRange<T, U, V> {
        TripleRange::new_partial(
            self.0.then_upto(limit.0),
            self.1.then_upto(limit.1),
            self.2.then_upto(limit.2),
            limit.1.upto(),
            limit.2.upto(),
        )
    }

    #[inline]
    fn to_non_zero(self) -> Option<Self::NonZero> {
        match (
            self.0.to_non_zero(),
            self.1.to_non_zero(),
            self.2.to_non_zero(),
        ) {
            (None, None, None) => None,
            (Some(lo), None, None) => {
                Some(NonZeroTriple(NonZeroTripleImpl::One(lo, self.1, self.2)))
            }
            (None, Some(md), None) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Two(self.0, md, self.2)))
            }
            (Some(lo), Some(md), None) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Three(lo, md, self.2)))
            }
            (None, None, Some(hi)) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Four(self.0, self.1, hi)))
            }
            (Some(lo), None, Some(hi)) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Five(lo, self.1, hi)))
            }
            (None, Some(md), Some(hi)) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Six(self.0, md, hi)))
            }
            (Some(lo), Some(md), Some(hi)) => {
                Some(NonZeroTriple(NonZeroTripleImpl::Seven(lo, md, hi)))
            }
        }
    }

    #[inline]
    fn from_non_zero(nz: Self::NonZero) -> Self {
        match nz.0 {
            NonZeroTripleImpl::One(lo, md, hi) => (T::from_non_zero(lo), md, hi),
            NonZeroTripleImpl::Two(lo, md, hi) => (lo, U::from_non_zero(md), hi),
            NonZeroTripleImpl::Three(lo, md, hi) => {
                (T::from_non_zero(lo), U::from_non_zero(md), hi)
            }
            NonZeroTripleImpl::Four(lo, md, hi) => (lo, md, V::from_non_zero(hi)),
            NonZeroTripleImpl::Five(lo, md, hi) => (T::from_non_zero(lo), md, V::from_non_zero(hi)),
            NonZeroTripleImpl::Six(lo, md, hi) => (lo, U::from_non_zero(md), V::from_non_zero(hi)),
            NonZeroTripleImpl::Seven(lo, md, hi) => (
                T::from_non_zero(lo),
                U::from_non_zero(md),
                V::from_non_zero(hi),
            ),
        }
    }
}
impl<T: Index, U: Index, V: Index, W: Index> Index for (T, U, V, W) {
    const MAX_INDEX: usize = <((T, U, V), W) as Index>::MAX_INDEX;

    type NonZero = NonZeroQuadruple<T, U, V, W>;
    type Iter = QuadrupleRange<T, U, V, W>;

    #[inline]
    fn to_index(self) -> usize {
        ((self.0, self.1, self.2), self.3).to_index()
    }

    #[inline]
    fn from_index(idx: usize) -> (T, U, V, W) {
        let ((lo1, lo2, hi1), hi2) = <((T, U, V), W)>::from_index(idx);
        (lo1, lo2, hi1, hi2)
    }

    #[inline]
    fn upto(self) -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange::new(self.0.upto(), self.1.upto(), self.2.upto(), self.3.upto())
    }

    #[inline]
    fn then_upto(self, limit: (T, U, V, W)) -> QuadrupleRange<T, U, V, W> {
        QuadrupleRange::new_partial(
            self.0.then_upto(limit.0),
            self.1.then_upto(limit.1),
            self.2.then_upto(limit.2),
            self.3.then_upto(limit.3),
            limit.1.upto(),
            limit.2.upto(),
            limit.3.upto(),
        )
    }

    #[inline]
    fn to_non_zero(self) -> Option<Self::NonZero> {
        match (
            self.0.to_non_zero(),
            self.1.to_non_zero(),
            self.2.to_non_zero(),
            self.3.to_non_zero(),
        ) {
            (None, None, None, None) => None,
            (Some(lo1), None, None, None) => Some(NonZeroQuadruple(NonZeroQuadrupleImpl::One(
                lo1, self.1, self.2, self.3,
            ))),
            (None, Some(lo2), None, None) => Some(NonZeroQuadruple(NonZeroQuadrupleImpl::Two(
                self.0, lo2, self.2, self.3,
            ))),
            (Some(lo1), Some(lo2), None, None) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Three(lo1, lo2, self.2, self.3),
            )),
            (None, None, Some(hi1), None) => Some(NonZeroQuadruple(NonZeroQuadrupleImpl::Four(
                self.0, self.1, hi1, self.3,
            ))),
            (Some(lo1), None, Some(hi1), None) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Five(lo1, self.1, hi1, self.3),
            )),
            (None, Some(lo2), Some(hi1), None) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Six(self.0, lo2, hi1, self.3),
            )),
            (Some(lo1), Some(lo2), Some(hi1), None) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Seven(lo1, lo2, hi1, self.3),
            )),
            (None, None, None, Some(hi2)) => Some(NonZeroQuadruple(NonZeroQuadrupleImpl::Eight(
                self.0, self.1, self.2, hi2,
            ))),
            (Some(lo1), None, None, Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Nine(lo1, self.1, self.2, hi2),
            )),
            (None, Some(lo2), None, Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Ten(self.0, lo2, self.2, hi2),
            )),
            (Some(lo1), Some(lo2), None, Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Eleven(lo1, lo2, self.2, hi2),
            )),
            (None, None, Some(hi1), Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Twelve(self.0, self.1, hi1, hi2),
            )),
            (Some(lo1), None, Some(hi1), Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Thirteen(lo1, self.1, hi1, hi2),
            )),
            (None, Some(lo2), Some(hi1), Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Fourteen(self.0, lo2, hi1, hi2),
            )),
            (Some(lo1), Some(lo2), Some(hi1), Some(hi2)) => Some(NonZeroQuadruple(
                NonZeroQuadrupleImpl::Fifteen(lo1, lo2, hi1, hi2),
            )),
        }
    }

    #[inline]
    fn from_non_zero(nz: Self::NonZero) -> Self {
        match nz.0 {
            NonZeroQuadrupleImpl::One(lo1, lo2, hi1, hi2) => (T::from_non_zero(lo1), lo2, hi1, hi2),
            NonZeroQuadrupleImpl::Two(lo1, lo2, hi1, hi2) => (lo1, U::from_non_zero(lo2), hi1, hi2),
            NonZeroQuadrupleImpl::Three(lo1, lo2, hi1, hi2) => {
                (T::from_non_zero(lo1), U::from_non_zero(lo2), hi1, hi2)
            }
            NonZeroQuadrupleImpl::Four(lo1, lo2, hi1, hi2) => {
                (lo1, lo2, V::from_non_zero(hi1), hi2)
            }
            NonZeroQuadrupleImpl::Five(lo1, lo2, hi1, hi2) => {
                (T::from_non_zero(lo1), lo2, V::from_non_zero(hi1), hi2)
            }
            NonZeroQuadrupleImpl::Six(lo1, lo2, hi1, hi2) => {
                (lo1, U::from_non_zero(lo2), V::from_non_zero(hi1), hi2)
            }
            NonZeroQuadrupleImpl::Seven(lo1, lo2, hi1, hi2) => (
                T::from_non_zero(lo1),
                U::from_non_zero(lo2),
                V::from_non_zero(hi1),
                hi2,
            ),
            NonZeroQuadrupleImpl::Eight(lo1, lo2, hi1, hi2) => {
                (lo1, lo2, hi1, W::from_non_zero(hi2))
            }
            NonZeroQuadrupleImpl::Nine(lo1, lo2, hi1, hi2) => {
                (T::from_non_zero(lo1), lo2, hi1, W::from_non_zero(hi2))
            }
            NonZeroQuadrupleImpl::Ten(lo1, lo2, hi1, hi2) => {
                (lo1, U::from_non_zero(lo2), hi1, W::from_non_zero(hi2))
            }
            NonZeroQuadrupleImpl::Eleven(lo1, lo2, hi1, hi2) => (
                T::from_non_zero(lo1),
                U::from_non_zero(lo2),
                hi1,
                W::from_non_zero(hi2),
            ),
            NonZeroQuadrupleImpl::Twelve(lo1, lo2, hi1, hi2) => {
                (lo1, lo2, V::from_non_zero(hi1), W::from_non_zero(hi2))
            }
            NonZeroQuadrupleImpl::Thirteen(lo1, lo2, hi1, hi2) => (
                T::from_non_zero(lo1),
                lo2,
                V::from_non_zero(hi1),
                W::from_non_zero(hi2),
            ),
            NonZeroQuadrupleImpl::Fourteen(lo1, lo2, hi1, hi2) => (
                lo1,
                U::from_non_zero(lo2),
                V::from_non_zero(hi1),
                W::from_non_zero(hi2),
            ),
            NonZeroQuadrupleImpl::Fifteen(lo1, lo2, hi1, hi2) => (
                T::from_non_zero(lo1),
                U::from_non_zero(lo2),
                V::from_non_zero(hi1),
                W::from_non_zero(hi2),
            ),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::scalars::Index;

    #[test]
    fn signed_to_index() {
        assert_eq!((0isize).to_index(), 0usize);
        assert_eq!((-1isize).to_index(), 1usize);
        assert_eq!((1isize).to_index(), 2usize);
        assert_eq!((-2isize).to_index(), 3usize);
        assert_eq!((2isize).to_index(), 4usize);
        assert_eq!(isize::MIN.to_index(), usize::MAX);
        assert_eq!(isize::MAX.to_index(), usize::MAX - 1);
    }

    #[test]
    fn signed_from_index() {
        assert_eq!(isize::from_index(0), 0isize);
        assert_eq!(isize::from_index(1), -1isize);
        assert_eq!(isize::from_index(2), 1isize);
        assert_eq!(isize::from_index(3), -2isize);
        assert_eq!(isize::from_index(4), 2isize);
        assert_eq!(isize::from_index(usize::MAX), isize::MIN);
        assert_eq!(isize::from_index(usize::MAX - 1), isize::MAX);
    }
}
