//! Iterator helpers.
#[cfg(feature = "alloc")]
use core::fmt;
use core::iter::{FusedIterator, Iterator as CoreIterator};

/// Trait alias for a "nice" iterator, used by various traits.
pub trait Iterator = CoreIterator + DoubleEndedIterator + ExactSizeIterator + FusedIterator;

/// Extension trait for iterators.
#[cfg(feature = "alloc")]
pub(crate) trait IteratorExt: CoreIterator {
    /// Make exact-size, given total length of iterator.
    #[inline]
    fn assume_len(self, len: usize) -> AssumeLen<Self>
    where
        Self: Sized,
    {
        AssumeLen { iter: self, len }
    }
}
#[cfg(feature = "alloc")]
impl<I: CoreIterator> IteratorExt for I {}

/// Assumes the length of an iterator, making it exact-size.
#[derive(Clone)]
#[cfg(feature = "alloc")]
pub(crate) struct AssumeLen<I> {
    /// Wrapped iterator.
    iter: I,

    /// Assumed length.
    len: usize,
}
#[cfg(feature = "alloc")]
impl<I: CoreIterator> CoreIterator for AssumeLen<I> {
    type Item = I::Item;

    #[inline]
    fn next(&mut self) -> Option<I::Item> {
        let ret = self.iter.next()?;
        self.len -= 1;
        Some(ret)
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}
#[cfg(feature = "alloc")]
impl<I: DoubleEndedIterator> DoubleEndedIterator for AssumeLen<I> {
    #[inline]
    fn next_back(&mut self) -> Option<Self::Item> {
        let ret = self.iter.next_back()?;
        self.len -= 1;
        Some(ret)
    }
}
#[cfg(feature = "alloc")]
impl<I: CoreIterator> ExactSizeIterator for AssumeLen<I> {
    #[inline]
    fn len(&self) -> usize {
        self.len
    }
}
#[cfg(feature = "alloc")]
impl<I: FusedIterator> FusedIterator for AssumeLen<I> {}
#[cfg(feature = "alloc")]
impl<I: fmt::Debug> fmt::Debug for AssumeLen<I> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.iter, f)?;
        write!(f, ".assume_len({})", self.len)
    }
}
