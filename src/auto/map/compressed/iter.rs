//! Iterator implementations for [`Compressed`].
//!
//! [`Compressed`]: crate::auto::map::compressed::Compressed
use core::{
    iter::{Flatten, FusedIterator},
    slice,
};

use crate::{
    auto::map::compressed::{CompressedSub, TransMeta},
    scalars::{BiStatus, Index, Input, State},
};

/// Virtual states for each real state.
#[derive(Clone, Debug)]
struct VirtStatuses<'a, S, I> {
    /// Real state.
    state: S,

    /// Current input size.
    index: usize,

    /// Inputs for state.
    inputs: &'a [I],

    /// Status of state.
    status: BiStatus,
}
impl<'a, S: State, I: Input> VirtStatuses<'a, S, I> {
    /// Makes iterator.
    fn new(state: S, inputs: &'a [I], status: BiStatus) -> VirtStatuses<'a, S, I> {
        VirtStatuses {
            state,
            index: 0,
            inputs,
            status,
        }
    }
}
impl<'a, S: State, I: Input> Iterator for VirtStatuses<'a, S, I> {
    type Item = ((S, &'a [I]), BiStatus);

    #[inline]
    fn next(&mut self) -> Option<((S, &'a [I]), BiStatus)> {
        let index = self.index;
        if index > self.inputs.len() {
            None
        } else {
            self.index += 1;
            Some(((self.state, &self.inputs[..index]), self.status))
        }
    }
}
impl<'a, S: State, I: Input> DoubleEndedIterator for VirtStatuses<'a, S, I> {
    #[inline]
    fn next_back(&mut self) -> Option<((S, &'a [I]), BiStatus)> {
        let inputs = self.inputs;
        if self.index > inputs.len() {
            None
        } else {
            self.inputs = self.inputs.split_last().map_or(&[], |(_, inputs)| inputs);
            Some(((self.state, inputs), self.status))
        }
    }
}

/// Nested virtual states a real state.
#[derive(Clone, Debug)]
struct NestedStatuses<'a, S, L: Index, I> {
    /// State.
    state: S,
    /// Status.
    status: BiStatus,

    /// Slice of inputs.
    inputs: &'a [I],

    /// Inner iterator.
    iter: slice::Iter<'a, TransMeta<S, L>>,
}
impl<'a, S, L: Index, I> NestedStatuses<'a, S, L, I> {
    /// Makes iterator.
    fn new(state: S, sub: &'a CompressedSub<S, L, I>) -> NestedStatuses<'a, S, L, I> {
        NestedStatuses {
            state,
            iter: sub.trans_states.iter(),
            inputs: &sub.trans_inputs,
            status: sub.status,
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for NestedStatuses<'a, S, L, I> {
    type Item = VirtStatuses<'a, S, I>;

    #[inline]
    fn next(&mut self) -> Option<VirtStatuses<'a, S, I>> {
        self.iter.next().map(|meta| {
            VirtStatuses::new(
                self.state,
                &self.inputs[meta.pos.to_index()..][..L::from_non_zero(meta.len).to_index()],
                self.status,
            )
        })
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for NestedStatuses<'a, S, L, I> {
    fn next_back(&mut self) -> Option<VirtStatuses<'a, S, I>> {
        self.iter.next_back().map(|meta| {
            VirtStatuses::new(
                self.state,
                &self.inputs[meta.pos.to_index()..][..L::from_non_zero(meta.len).to_index()],
                self.status,
            )
        })
    }
}

/// Nested virtual states for each real state.
#[derive(Clone, Debug)]
struct DoublyNestedStatuses<'a, S, L: Index, I> {
    /// Inner iterator.
    iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
}
impl<'a, S, L: Index, I> DoublyNestedStatuses<'a, S, L, I> {
    /// Makes iterator.
    fn new(
        iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
    ) -> DoublyNestedStatuses<'a, S, L, I> {
        DoublyNestedStatuses { iter }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for DoublyNestedStatuses<'a, S, L, I> {
    type Item = Flatten<NestedStatuses<'a, S, L, I>>;

    #[inline]
    fn next(&mut self) -> Option<Flatten<NestedStatuses<'a, S, L, I>>> {
        self.iter
            .next()
            .map(|(s, map)| NestedStatuses::new(*s, map).flatten())
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for DoublyNestedStatuses<'a, S, L, I> {
    #[inline]
    fn next_back(&mut self) -> Option<Flatten<NestedStatuses<'a, S, L, I>>> {
        self.iter
            .next_back()
            .map(|(s, map)| NestedStatuses::new(*s, map).flatten())
    }
}

/// Virtual states in the automaton.
#[derive(Clone, Debug)]
pub struct Statuses<'a, S: State, L: Index, I: Input> {
    /// Inner iterator.
    iter: Flatten<DoublyNestedStatuses<'a, S, L, I>>,

    /// Since we can't easily get the length of the flattened iterator, we store it explicitly.
    len: usize,
}
impl<'a, S: State, L: Index, I: Input> Statuses<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
        len: usize,
    ) -> Statuses<'a, S, L, I> {
        Statuses {
            iter: DoublyNestedStatuses::new(iter).flatten(),
            len,
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for Statuses<'a, S, L, I> {
    type Item = ((S, &'a [I]), BiStatus);

    fn next(&mut self) -> Option<((S, &'a [I]), BiStatus)> {
        let ret = self.iter.next()?;
        self.len -= 1;
        Some(ret)
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for Statuses<'a, S, L, I> {
    fn next_back(&mut self) -> Option<((S, &'a [I]), BiStatus)> {
        let ret = self.iter.next_back()?;
        self.len -= 1;
        Some(ret)
    }
}
impl<S: State, L: Index, I: Input> ExactSizeIterator for Statuses<'_, S, L, I> {
    fn len(&self) -> usize {
        self.len
    }
}
impl<S: State, L: Index, I: Input> FusedIterator for Statuses<'_, S, L, I> {}

/// Transes.
#[derive(Clone, Debug)]
pub struct Transes<'a, S, L: Index, I> {
    /// Initial state.
    state: S,

    /// Length of initial state inputs.
    len: usize,

    /// Input data slice.
    inputs: &'a [I],

    /// Iterator over metadata for transitions.
    iter: slice::Iter<'a, TransMeta<S, L>>,
}
impl<'a, S: State, L: Index, I: Input> Transes<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        state: S,
        sub: &'a CompressedSub<S, L, I>,
        inputs: &'a [I],
    ) -> Transes<'a, S, L, I> {
        let iter = sub.binary_search_trans(inputs).err().unwrap_or(&[]).iter();
        Transes {
            state,
            len: inputs.len(),
            iter,
            inputs: &sub.trans_inputs,
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for Transes<'a, S, L, I> {
    type Item = (I, (S, &'a [I]));

    fn next(&mut self) -> Option<(I, (S, &'a [I]))> {
        let meta = self.iter.next()?;
        let mut slice = meta.slice(self.inputs);

        let input = slice[self.len];
        let state;

        // verify that we're not just advancing to a virtual state, and going to an actual, new
        // state
        if slice.len() == self.len + 1 {
            state = meta.dest;
            slice = &[];
        } else {
            state = self.state;
            slice = &slice[..=self.len];
        }

        Some((input, (state, slice)))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for Transes<'a, S, L, I> {
    fn next_back(&mut self) -> Option<(I, (S, &'a [I]))> {
        let meta = self.iter.next_back()?;
        let mut slice = meta.slice(self.inputs);

        let input = slice[self.len];
        let state;

        // verify that we're not just advancing to a virtual state, and going to an actual, new
        // state
        if slice.len() == self.len + 1 {
            state = meta.dest;
            slice = &[];
        } else {
            state = self.state;
            slice = &slice[..=self.len];
        }

        Some((input, (state, slice)))
    }
}
impl<S: State, L: Index, I: Input> ExactSizeIterator for Transes<'_, S, L, I> {
    #[inline]
    fn len(&self) -> usize {
        self.iter.len()
    }
}
impl<S: State, L: Index, I: Input> FusedIterator for Transes<'_, S, L, I> {}

/// Virtual transitions for a single real transition.
#[derive(Clone, Debug)]
pub(super) struct VirtualTranses<'a, S, L: Index, I> {
    /// Source real state
    src: S,

    /// Destination real state.
    dest: S,

    /// Input data slice.
    inputs: &'a [I],

    /// Range of indices.
    iter: L::Iter,
}
impl<'a, S: State, L: Index, I: Input> VirtualTranses<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        src: S,
        meta: TransMeta<S, L>,
        inputs: &'a [I],
    ) -> VirtualTranses<'a, S, L, I> {
        VirtualTranses {
            src,
            dest: meta.dest,
            inputs: &inputs[meta.pos.to_index()..],
            iter: L::from_non_zero(meta.len).upto(),
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for VirtualTranses<'a, S, L, I> {
    type Item = ((S, &'a [I]), I, (S, &'a [I]));

    fn next(&mut self) -> Option<((S, &'a [I]), I, (S, &'a [I]))> {
        let len = self.iter.next()?.to_index();
        if self.iter.len() == 0 {
            Some((
                (self.src, &self.inputs[..len]),
                self.inputs[len],
                (self.dest, &[]),
            ))
        } else {
            Some((
                (self.src, &self.inputs[..len]),
                self.inputs[len],
                (self.dest, &self.inputs[..=len]),
            ))
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for VirtualTranses<'a, S, L, I> {
    fn next_back(&mut self) -> Option<((S, &'a [I]), I, (S, &'a [I]))> {
        let len = self.iter.next_back()?.to_index();
        if self.iter.len() == 0 {
            Some((
                (self.src, &self.inputs[..len]),
                self.inputs[len],
                (self.dest, &[]),
            ))
        } else {
            Some((
                (self.src, &self.inputs[..len]),
                self.inputs[len],
                (self.dest, &self.inputs[..=len]),
            ))
        }
    }
}

/// All transitions for a single state.
#[derive(Clone, Debug)]
pub(super) struct NestedTranses<'a, S, L: Index, I> {
    /// Starting state for transition.
    state: S,

    /// Inputs data slice.
    inputs: &'a [I],

    /// Iterator over meta transitions.
    iter: slice::Iter<'a, TransMeta<S, L>>,
}
impl<'a, S: State, L: Index, I: Input> NestedTranses<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        state: S,
        inputs: &'a [I],
        iter: slice::Iter<'a, TransMeta<S, L>>,
    ) -> NestedTranses<'a, S, L, I> {
        NestedTranses {
            state,
            inputs,
            iter,
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for NestedTranses<'a, S, L, I> {
    type Item = VirtualTranses<'a, S, L, I>;

    fn next(&mut self) -> Option<VirtualTranses<'a, S, L, I>> {
        let meta = self.iter.next()?;
        Some(VirtualTranses::new(self.state, *meta, self.inputs))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for NestedTranses<'a, S, L, I> {
    fn next_back(&mut self) -> Option<VirtualTranses<'a, S, L, I>> {
        let meta = self.iter.next_back()?;
        Some(VirtualTranses::new(self.state, *meta, self.inputs))
    }
}

/// Nested transitions for all states.
#[derive(Clone, Debug)]
pub(super) struct DoublyNestedTranses<'a, S, L: Index, I> {
    /// Iterator over states.
    iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
}
impl<'a, S: State, L: Index, I: Input> DoublyNestedTranses<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
    ) -> DoublyNestedTranses<'a, S, L, I> {
        DoublyNestedTranses { iter }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for DoublyNestedTranses<'a, S, L, I> {
    type Item = Flatten<NestedTranses<'a, S, L, I>>;

    fn next(&mut self) -> Option<Flatten<NestedTranses<'a, S, L, I>>> {
        let (state, sub) = self.iter.next()?;
        Some(NestedTranses::new(*state, &sub.trans_inputs, sub.trans_states.iter()).flatten())
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for DoublyNestedTranses<'a, S, L, I> {
    fn next_back(&mut self) -> Option<Flatten<NestedTranses<'a, S, L, I>>> {
        let (state, sub) = self.iter.next_back()?;
        Some(NestedTranses::new(*state, &sub.trans_inputs, sub.trans_states.iter()).flatten())
    }
}

/// Transitions.
#[derive(Clone, Debug)]
pub struct Transitions<'a, S: State, L: Index, I: Input> {
    /// Inner iterator.
    iter: Flatten<DoublyNestedTranses<'a, S, L, I>>,

    /// Since we can't easily get the length of the flattened iterator, we store it explicitly.
    len: usize,
}
impl<'a, S: State, L: Index, I: Input> Transitions<'a, S, L, I> {
    /// Makes iterator.
    pub(super) fn new(
        iter: indexmap::map::Iter<'a, S, CompressedSub<S, L, I>>,
        len: usize,
    ) -> Transitions<'a, S, L, I> {
        Transitions {
            iter: DoublyNestedTranses::new(iter).flatten(),
            len,
        }
    }
}
impl<'a, S: State, L: Index, I: Input> Iterator for Transitions<'a, S, L, I> {
    type Item = ((S, &'a [I]), I, (S, &'a [I]));

    fn next(&mut self) -> Option<((S, &'a [I]), I, (S, &'a [I]))> {
        let ret = self.iter.next()?;
        self.len -= 1;
        Some(ret)
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
impl<'a, S: State, L: Index, I: Input> DoubleEndedIterator for Transitions<'a, S, L, I> {
    fn next_back(&mut self) -> Option<((S, &'a [I]), I, (S, &'a [I]))> {
        self.iter.next_back()
    }
}
impl<S: State, L: Index, I: Input> ExactSizeIterator for Transitions<'_, S, L, I> {
    fn len(&self) -> usize {
        self.len
    }
}
impl<S: State, L: Index, I: Input> FusedIterator for Transitions<'_, S, L, I> {}
