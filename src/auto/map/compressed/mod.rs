//! [`Compressed`] version of [`Map`].
//!
//! [`Map`]: crate::auto::Map
use alloc::vec::Vec;
use core::{cmp::Ordering, marker::PhantomData};

use indexmap::IndexMap;

use crate::{
    auto::{Automaton, map::state_oob},
    hash::BuildHasher,
    scalars::{BiStatus, Index, Input, State},
};

mod iter;
pub(super) use self::iter::{Statuses, Transes, Transitions};

/// Metadata about transition.
#[derive(Copy, Clone, Debug)]
struct TransMeta<S, L: Index> {
    /// Destination of transition.
    dest: S,

    /// Index of input data for transition.
    pos: L,

    /// Length of data for transition vec.
    len: L::NonZero,
}
impl<S, L: Index> TransMeta<S, L> {
    /// Slice into inputs.
    fn slice<'i, I>(&self, inputs: &'i [I]) -> &'i [I] {
        &inputs[self.pos.to_index()..][..L::from_non_zero(self.len).to_index()]
    }
}

/// Data for compressed automaton for a single real state.
#[derive(Clone, Debug)]
struct CompressedSub<S, L: Index, I> {
    /// Status of state.
    status: BiStatus,

    /// State halves of transitions.
    trans_states: Vec<TransMeta<S, L>>,

    /// Input halves for transitions.
    trans_inputs: Vec<I>,
}
impl<S: State, L: Index, I: Input> CompressedSub<S, L, I> {
    /// Find the transitions that start with the given inputs.
    fn binary_search_trans(&self, inputs: &[I]) -> Result<&TransMeta<S, L>, &[TransMeta<S, L>]> {
        // short-circuit for an empty slice
        if inputs.is_empty() {
            return Err(&self.trans_states);
        }

        // look through transitions to find ones where inputs match prefix
        match self
            .trans_states
            .binary_search_by_key(&inputs, |meta| meta.slice(&self.trans_inputs))
        {
            Ok(exact) => Ok(&self.trans_states[exact]),
            Err(inexact) => {
                // if we don't have an exact match, we'll still be lexicographically before any ones
                // where it's a prefix. binary search to find the last one where it's a prefix
                let hi = self.trans_states[inexact..]
                    .binary_search_by(|meta| {
                        let slice = meta.slice(&self.trans_inputs);
                        if slice.starts_with(inputs) {
                            Ordering::Greater
                        } else {
                            Ordering::Less
                        }
                    })
                    .expect_err("binary_search_by function never returns Equal");

                Err(&self.trans_states[inexact..=hi])
            }
        }
    }
}

/// Compressed automaton.
#[derive(Clone, Debug)]
pub struct Compressed<
    S,
    L: Index,
    I,
    #[cfg(feature = "std")] H = std::hash::RandomState,
    #[cfg(not(feature = "std"))] H,
> {
    /// Map of states to data.
    trans: IndexMap<S, CompressedSub<S, L, I>, H>,

    /// Marker for state length.
    marker: PhantomData<L>,

    /// Total number of virtual states in map.
    len_states: usize,

    /// Total number of transitions in map.
    len_trans: usize,
}
impl<S: State, L: Index, I: Input, H: BuildHasher> Compressed<S, L, I, H> {
    /// Gets the sub-map for a given state.
    fn get(&self, state: S) -> &CompressedSub<S, L, I> {
        self.trans.get(&state).unwrap_or_else(|| state_oob(state))
    }
}

#[allow(unused)]
impl<'a, S: State, L: Index, I: Input, H: BuildHasher> Automaton for &'a Compressed<S, L, I, H> {
    type State = (S, &'a [I]);
    type Input = I;

    #[inline]
    fn status(&self, (state, _): (S, &'a [I])) -> BiStatus {
        self.get(state).status
    }

    #[inline]
    fn statuses(&self) -> Statuses<'a, S, L, I> {
        Statuses::new(self.trans.iter(), self.len_states)
    }

    #[inline]
    fn trans(&self, (mut state, init): (S, &'a [I]), last: I) -> Option<(S, &'a [I])> {
        let sub = self.get(state);

        // find transitions that include init, but are nonempty
        let metas = sub.binary_search_trans(init).err()?;

        // find transitions that include last
        let idx = metas
            .binary_search_by(|meta| {
                let slice = meta.slice(&sub.trans_inputs);

                // binary_search_trans will only include slices larger than the init slice on Err
                last.cmp(&slice[init.len()])
            })
            .ok()?;

        // it doesn't matter which one we found, as long as it's valid
        let meta = &metas[idx];
        let mut slice = meta.slice(&sub.trans_inputs);

        // we're advancing to a new real state
        if slice.len() == init.len() + 1 {
            state = meta.dest;
            slice = &[];
        } else {
            slice = &slice[..=init.len()];
        }

        Some((state, slice))
    }

    #[inline]
    fn transes(&self, (state, inputs): (S, &'a [I])) -> Transes<'a, S, L, I> {
        Transes::new(state, self.get(state), inputs)
    }

    #[inline]
    fn transitions(&self) -> Transitions<'a, S, L, I> {
        Transitions::new(self.trans.iter(), self.len_trans)
    }

    #[inline]
    fn trans_slice(
        &self,
        (mut state, mut start): (S, &'a [I]),
        end: &[I],
    ) -> ((S, &'a [I]), usize) {
        let mut count = 0;
        loop {
            let sub = self.get(state);

            // find transitions that include start, but are nonempty
            let Err(metas) = sub.binary_search_trans(start) else {
                return ((state, start), count);
            };

            // find transitions that include end
            let idx = metas.binary_search_by(|meta| {
                // binary_search_trans will only include slices larger than the start slice on Err
                let slice = &meta.slice(&sub.trans_inputs)[start.len()..];
                let mut end_check = end;

                // clamp end to given slice;
                // it's okay if we don't use all of it, since we still change state on exact match
                if end.len() < slice.len() {
                    end_check = &end[..slice.len()];
                }

                end_check.cmp(slice)
            });

            match idx {
                // complete transition
                Ok(idx) => {
                    let meta = &metas[idx];
                    state = meta.dest;
                    start = &[];
                    count += meta.slice(&sub.trans_inputs).len() - start.len();
                }

                // incomplete transition
                Err(idx) => {
                    let meta = &metas[idx];
                    let slice = meta.slice(&sub.trans_inputs);

                    todo!();
                    return ((state, start), count);
                }
            }
        }
    }

    #[inline]
    fn trans_iter<Iter: IntoIterator<Item = I>>(
        &self,
        (state, index): (S, &'a [I]),
        inputs: Iter,
    ) -> ((S, &'a [I]), usize) {
        todo!()
    }
}
