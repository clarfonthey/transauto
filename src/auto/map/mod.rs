//! [`Map`] implementation of [`Automaton`].

use core::{cmp::Ordering, fmt, mem};

use indexmap::{IndexMap, IndexSet, map::Entry};

use crate::{
    auto::{Automaton, AutomatonBuilder, AutomatonMut, BiAutomaton},
    hash::BuildHasher,
    iter::{Iterator, IteratorExt},
    scalars::{BiStatus, Input, State, Status},
};

mod compressed;

/// Inverse transition.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct InvTrans<S, I> {
    /// Input for transition.
    input: I,

    /// State for transition.
    state: S,
}
impl<S: State, I: Input> InvTrans<S, I> {
    /// Gets as a (state, input) pair.
    #[inline]
    pub(crate) fn pair(&self) -> (S, I) {
        (self.state, self.input)
    }
}
impl<S: fmt::Debug, I: fmt::Debug> fmt::Debug for InvTrans<S, I> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("")
            .field(&self.state)
            .field(&self.input)
            .finish()
    }
}

/// Properties for a state.
#[derive(Clone, Debug)]
struct SubMap<
    S,
    I,
    #[cfg(feature = "std")] H = std::hash::RandomState,
    #[cfg(not(feature = "std"))] H,
> {
    /// Status of state.
    status: BiStatus,

    /// Transition map.
    trans: IndexMap<I, S, H>,

    /// Inverse transitions.
    inv: IndexSet<InvTrans<S, I>, H>,
}
impl<S: State, I: Input, H: BuildHasher> SubMap<S, I, H> {
    /// Creates a state with the given status and hasher.
    pub(crate) fn new(status: BiStatus, hasher: H) -> SubMap<S, I, H> {
        SubMap {
            status,
            trans: IndexMap::with_hasher(hasher.clone()),
            inv: IndexSet::with_hasher(hasher),
        }
    }

    /// Gets a transition for an input.
    pub(crate) fn trans(&self, input: I) -> Option<S> {
        self.trans.get(&input).copied()
    }

    /// Gets the first transition.
    pub(crate) fn first_trans(&self) -> Option<(I, S)> {
        self.trans.first().map(|(input, state)| (*input, *state))
    }

    /// Gets the last transition.
    pub(crate) fn last_trans(&self) -> Option<(I, S)> {
        self.trans.last().map(|(input, state)| (*input, *state))
    }

    /// Gets the cis range for an input.
    pub(crate) fn cis(&self, input: I) -> &indexmap::set::Slice<InvTrans<S, I>> {
        let lo = self
            .inv
            .binary_search_by(|inv| inv.input.cmp(&input).then(Ordering::Less))
            .unwrap_err();
        let hi = self.inv[lo..]
            .binary_search_by(|inv| inv.input.cmp(&input).then(Ordering::Greater))
            .unwrap_err();
        let range = if lo + 1 < hi { lo + 1..hi } else { 0..0 };
        &self.inv[range]
    }
}

/// Cold panic message when state is out of bounds.
#[cold]
fn state_oob<S: State>(state: S) -> ! {
    panic!("state {state:?} out of bounds")
}

/// Simple [`AutomatonBuilder`].
#[derive(Clone, Debug)]
pub struct Map<
    S,
    I,
    #[cfg(feature = "std")] H = std::hash::RandomState,
    #[cfg(not(feature = "std"))] H,
> {
    /// State map.
    trans: IndexMap<S, SubMap<S, I, H>, H>,

    /// Total number of transitions in map.
    len: usize,
}
impl<S: State, I: Input, H: BuildHasher> Map<S, I, H> {
    /// Gets the sub-map for a given state.
    fn get(&self, state: S) -> &SubMap<S, I, H> {
        self.trans.get(&state).unwrap_or_else(|| state_oob(state))
    }

    /// Gets the index of the sub-map for a given state.
    fn get_index(&self, state: S) -> usize {
        self.trans
            .get_index_of(&state)
            .unwrap_or_else(|| state_oob(state))
    }

    /// Write graphviz dot file.
    pub fn dot(&self) -> impl '_ + fmt::Display {
        /// Sub-implementation.
        struct DisplayMe<'a, S: State, I: Input, H: BuildHasher>(&'a Map<S, I, H>);
        impl<S: State, I: Input, H: BuildHasher> fmt::Display for DisplayMe<'_, S, I, H> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                writeln!(f, r#"digraph {{"#)?;
                writeln!(f, r#"    "A" [shape=doublecircle];"#)?;
                writeln!(f, r#"    "R" [shape=circle];"#)?;
                writeln!(f, r#"    "A" -> "A";"#)?;
                writeln!(f, r#"    "R" -> "R";"#)?;
                for (state, map) in self.0.trans.iter() {
                    let kind = match map.status.stay() {
                        Status::Accept => "doublecircle",
                        Status::Reject => "circle",
                    };
                    writeln!(f, r#"    "{state:?}" [shape={kind}];"#)?;

                    for (input, next) in map.trans.iter() {
                        writeln!(f, r#"    "{state:?}" -> "{next:?}" [label = "{input:?}"];"#)?;
                    }
                    let sink = match map.status.leave() {
                        Status::Accept => "A",
                        Status::Reject => "R",
                    };
                    writeln!(f, r#"    "{state:?}" -> "{sink}";"#)?;
                }
                writeln!(f, "}}")
            }
        }
        DisplayMe(self)
    }
}
impl<S: State, I: Input, H: BuildHasher> Automaton for Map<S, I, H> {
    type State = S;
    type Input = I;

    #[inline]
    fn status(&self, state: S) -> BiStatus {
        self.get(state).status
    }

    #[inline]
    fn statuses(&self) -> impl Clone + fmt::Debug + Iterator<Item = (S, BiStatus)> {
        self.trans.iter().map(|(state, map)| (*state, map.status))
    }

    #[inline]
    fn trans(&self, state: S, input: I) -> Option<S> {
        self.get(state).trans(input)
    }

    #[inline]
    fn first_trans(&self, state: S) -> Option<(I, S)> {
        self.get(state).first_trans()
    }

    #[inline]
    fn last_trans(&self, state: S) -> Option<(I, S)> {
        self.get(state).last_trans()
    }

    #[inline]
    fn transes(&self, state: S) -> impl Clone + fmt::Debug + Iterator<Item = (I, S)> {
        self.get(state)
            .trans
            .iter()
            .map(|(input, state)| (*input, *state))
    }

    #[inline]
    fn transitions(&self) -> impl Clone + fmt::Debug + Iterator<Item = (S, I, S)> {
        self.trans
            .iter()
            .flat_map(|(src, map)| map.trans.iter().map(|(input, dest)| (*src, *input, *dest)))
            .assume_len(self.len)
    }
}
impl<S: State, I: Input, H: BuildHasher> BiAutomaton for Map<S, I, H> {
    #[inline]
    fn cis(&self, input: I, state: S) -> impl Clone + fmt::Debug + Iterator<Item = S> {
        self.get(state).cis(input).iter().map(|inv| inv.state)
    }

    #[inline]
    fn cises(&self, state: S) -> impl Clone + fmt::Debug + Iterator<Item = (S, I)> {
        self.get(state).inv.iter().map(InvTrans::pair)
    }
}
impl<S: State, I: Input, H: BuildHasher> AutomatonMut for Map<S, I, H> {
    #[inline]
    fn set_status(&mut self, state: S, status: BiStatus) -> Option<BiStatus> {
        let hasher = self.trans.hasher().clone();
        match self.trans.entry(state) {
            Entry::Occupied(entry) => Some(mem::replace(&mut entry.into_mut().status, status)),
            Entry::Vacant(entry) => {
                entry.insert_sorted(SubMap::new(status, hasher));
                None
            }
        }
    }

    #[inline]
    fn set_trans(&mut self, state: S, input: I, next: S) -> Option<S> {
        let state_idx = self.get_index(state);

        let next_idx = self.get_index(next);

        let old = match self.trans[state_idx].trans.entry(input) {
            Entry::Occupied(entry) => Some(mem::replace(entry.into_mut(), next)),
            Entry::Vacant(entry) => {
                self.len += 1;
                entry.insert_sorted(next);
                None
            }
        };
        self.trans[next_idx]
            .inv
            .insert_sorted(InvTrans { input, state });
        if let Some(old) = old {
            if old != next {
                self.trans
                    .get_mut(&old)
                    .unwrap_or_else(|| unreachable!())
                    .inv
                    .shift_remove(&InvTrans { input, state });
            }
        }
        old
    }

    #[inline]
    fn remove(&mut self, state: S) -> bool {
        let Entry::Occupied(old) = self.trans.entry(state) else {
            return false;
        };

        let old = old.shift_remove();

        for (input, next) in old.trans {
            self.len -= 1;
            if next == state {
                continue;
            }
            self.trans
                .get_mut(&next)
                .unwrap_or_else(|| unreachable!())
                .inv
                .shift_remove(&InvTrans { input, state });
        }
        for InvTrans { state: prev, input } in old.inv {
            if prev == state {
                continue;
            }
            self.len -= 1;
            self.trans
                .get_mut(&prev)
                .unwrap_or_else(|| unreachable!())
                .trans
                .shift_remove(&input);
        }

        true
    }

    #[inline]
    fn replace(&mut self, src: S, dest: S) {
        if src == dest {
            return;
        }

        let dest_idx = self.get_index(dest);
        let src_idx = self.get_index(src);

        self.trans[dest_idx].status = self.trans[src_idx].status;

        // for every transition from the source
        for idx in 0..self.trans[src_idx].trans.len() {
            let input = self.trans[src_idx].trans.keys()[idx];
            let mut state = self.trans[src_idx].trans[idx];
            if state == src {
                state = dest;
            }

            // if the destination has that transition, remove its inverse;
            // the source has priority
            if let Some(&next) = self.trans[dest_idx].trans.get(&input) {
                self.len -= 1;
                self.trans
                    .get_mut(&next)
                    .unwrap_or_else(|| unreachable!())
                    .inv
                    .shift_remove(&InvTrans { state: dest, input });
            }

            // transition there from the destination
            self.trans[dest_idx].trans.insert(input, state);

            // and update the inverse-transition too
            let inv = &mut self
                .trans
                .get_mut(&state)
                .unwrap_or_else(|| unreachable!())
                .inv;
            inv.shift_remove(&InvTrans { state: src, input });
            inv.insert(InvTrans { state: dest, input });
        }

        // for every transition to the source
        for idx in 0..self.trans[src_idx].inv.len() {
            let InvTrans { mut state, input } = self.trans[src_idx].inv[idx];
            if state == src {
                state = dest;
            }

            // add that transition to the destination
            self.trans[dest_idx].inv.insert(InvTrans { input, state });

            // and update the inverse-transition too
            self.trans
                .get_mut(&state)
                .unwrap_or_else(|| unreachable!())
                .trans
                .insert(input, dest);
        }

        // finally, remove the source
        self.trans.shift_remove_index(src_idx);
    }

    #[inline]
    fn rename(&mut self, src: S, dest: S) {
        if self.trans.contains_key(&dest) {
            panic!("cannot overwrite state {dest:?}");
        }

        let hasher = self.trans.hasher().clone();
        let status = self.get(src).status;

        match self.trans.entry(dest) {
            Entry::Occupied(_) => {
                panic!("cannot overwrite state {dest:?}");
            }
            Entry::Vacant(entry) => {
                entry.insert_sorted(SubMap::new(status, hasher));
            }
        }

        let src_idx = self.get_index(src);
        let dest_idx = self.get_index(dest);

        // move stuff to dest; we have to do this for weird lifetime shenanigans
        let (dest_map, src_map) = if src_idx < dest_idx {
            let (has_src, has_dest) = self.trans.as_mut_slice().split_at_mut(dest_idx);
            (&mut has_src[src_idx], &mut has_dest[0])
        } else {
            let (has_dest, has_src) = self.trans.as_mut_slice().split_at_mut(src_idx);
            (&mut has_dest[dest_idx], &mut has_src[0])
        };
        dest_map.trans.append(&mut src_map.trans);
        dest_map.inv.append(&mut src_map.inv);

        // update inverse transitions
        for inv_idx in 0..dest_map.inv.len() {
            let inv_trans = self.trans[dest_idx].inv[inv_idx];
            let input = inv_trans.input;
            let state = inv_trans.state;

            let inv = &mut self
                .trans
                .get_mut(&state)
                .unwrap_or_else(|| unreachable!())
                .inv;
            inv.shift_remove(&InvTrans { state: src, input });
            inv.insert(InvTrans { state: dest, input });
        }

        // finally, remove the source
        self.trans.shift_remove_index(src_idx);
    }
}
impl<S: State, I: Input, H: BuildHasher> AutomatonBuilder for Map<S, I, H> {
    #[inline]
    fn new(status: BiStatus) -> Map<S, I, H> {
        let hasher = H::default();
        let mut trans = IndexMap::with_hasher(hasher.clone());
        trans.insert(State::STARTING, SubMap::new(status, hasher));
        Map { trans, len: 0 }
    }

    #[inline]
    fn from_parts(
        statuses: impl IntoIterator<Item = (S, BiStatus)>,
        transitions: impl IntoIterator<Item = (S, I, S)>,
    ) -> Map<S, I, H> {
        let mut map = Map {
            trans: IndexMap::with_hasher(H::default()),
            len: 0,
        };
        for (state, status) in statuses {
            map.set_status(state, status);
        }
        for (state, input, next) in transitions {
            map.set_trans(state, input, next);
        }
        map
    }

    // FIXME: Clone
    #[inline]
    fn into_statuses(self) -> impl Clone + fmt::Debug + Iterator<Item = (S, BiStatus)> {
        self.trans
            .into_iter()
            .map(|(state, map)| (state, map.status))
    }

    #[inline]
    fn into_transitions(self) -> impl Clone + fmt::Debug + Iterator<Item = (S, I, S)> {
        self.trans
            .into_iter()
            .flat_map(|(src, map)| {
                map.trans
                    .into_iter()
                    .map(move |(input, dest)| (src, input, dest))
            })
            .assume_len(self.len)
    }

    #[inline]
    fn into_parts(
        self,
    ) -> impl Clone + fmt::Debug + Iterator<Item = (S, BiStatus, impl Iterator<Item = (I, S)>)>
    {
        self.trans
            .into_iter()
            .map(move |(state, map)| (state, map.status, map.trans.into_iter()))
    }
}

#[cfg(test)]
mod tests {
    use core::{fmt, hash::BuildHasher, iter};
    use std::hash::RandomState;

    use indexmap::IndexSet;
    use rand::{
        Rng,
        distr::{Alphanumeric, Uniform},
    };

    use crate::{
        auto::{AutomatonBuilder, AutomatonMut, Map, map::InvTrans},
        scalars::{BiStatus, Input, State, Status},
    };

    fn verify_inv_matches<S: State, I: Input, H: BuildHasher>(
        fwd: &mut IndexSet<(S, I, S)>,
        bwd: &mut IndexSet<(S, I, S)>,
        map: &Map<S, I, H>,
        fmt: fmt::Arguments,
    ) {
        let fwd_old = fwd.drain(..).collect::<IndexSet<_>>();
        let bwd_old = bwd.drain(..).collect::<IndexSet<_>>();

        for (state, map) in map.trans.iter() {
            for (input, next) in map.trans.iter() {
                fwd.insert_sorted((*state, *input, *next));
            }
            for InvTrans { state: prev, input } in map.inv.iter() {
                bwd.insert_sorted((*prev, *input, *state));
            }
        }

        if fwd != bwd || map.len != fwd.len() {
            panic!(
                "\
                failed {:?}; \n\
                |MAP| = {:?}; \n\
                |FWD| = {:?}; \n\
                |BWD| = {:?}; \n\
                FWD O = {:?}; \n\
                BWD O = {:?}; \n\
                FWD A = {:?}; \n\
                FWD R = {:?}; \n\
                BWD A = {:?}; \n\
                BWD R = {:?}; \n\
                ",
                fmt,
                map.len,
                fwd.len(),
                bwd.len(),
                fwd.difference(bwd),
                bwd.difference(fwd),
                fwd.difference(&fwd_old),
                fwd_old.difference(fwd),
                bwd.difference(&bwd_old),
                bwd_old.difference(bwd),
            );
        }
    }

    #[test]
    fn simple() {
        let orig_statuses = [
            (1u8, BiStatus::RejectAll),
            (2u8, BiStatus::RejectAll),
            (3u8, BiStatus::RejectAll),
        ];
        let orig_transitions = [(1, 'a', 2), (2, 'b', 3), (3, 'c', 1)];
        let map = <Map<_, _, RandomState>>::from_parts(
            orig_statuses.iter().copied(),
            orig_transitions.iter().copied(),
        );
        verify_inv_matches(
            &mut IndexSet::new(),
            &mut IndexSet::new(),
            &map,
            format_args!("simple test"),
        );
        let mut statuses = map.clone().into_statuses().collect::<Vec<_>>();
        let mut transitions = map.into_transitions().collect::<Vec<_>>();
        statuses.sort();
        transitions.sort();
        assert_eq!(statuses, orig_statuses);
        assert_eq!(transitions, orig_transitions);
    }

    #[test]
    fn simple_replace() {
        let orig_statuses = [
            (1u8, BiStatus::RejectAll),
            (2u8, BiStatus::RejectAll),
            (3u8, BiStatus::RejectAll),
        ];
        let orig_transitions = [
            (1, 'a', 2),
            (2, 'b', 3),
            (3, 'c', 1),
            (3, 'a', 2),
            (3, 'd', 2),
        ];
        let mut map = <Map<_, _, RandomState>>::from_parts(
            orig_statuses.iter().copied(),
            orig_transitions.iter().copied(),
        );
        map.replace(1, 3);
        verify_inv_matches(
            &mut IndexSet::new(),
            &mut IndexSet::new(),
            &map,
            format_args!("simple test"),
        );
        let new_statuses = [(2u8, BiStatus::RejectAll), (3u8, BiStatus::RejectAll)];
        let new_transitions = [(2, 'b', 3), (3, 'a', 2), (3, 'c', 3), (3, 'd', 2)];
        let mut statuses = map.clone().into_statuses().collect::<Vec<_>>();
        let mut transitions = map.into_transitions().collect::<Vec<_>>();
        statuses.sort();
        transitions.sort();
        assert_eq!(statuses, new_statuses);
        assert_eq!(transitions, new_transitions);
    }

    #[test]
    fn simple_remove() {
        let orig_statuses = [
            (1u8, BiStatus::RejectAll),
            (2u8, BiStatus::RejectAll),
            (3u8, BiStatus::RejectAll),
        ];
        let orig_transitions = [(1, 'a', 2), (2, 'b', 3), (3, 'c', 1)];
        let mut map = <Map<_, _, RandomState>>::from_parts(
            orig_statuses.iter().copied(),
            orig_transitions.iter().copied(),
        );
        map.remove(2);
        verify_inv_matches(
            &mut IndexSet::new(),
            &mut IndexSet::new(),
            &map,
            format_args!("simple test"),
        );
        let new_statuses = [(1u8, BiStatus::RejectAll), (3u8, BiStatus::RejectAll)];
        let new_transitions = [(3, 'c', 1)];
        let mut statuses = map.clone().into_statuses().collect::<Vec<_>>();
        let mut transitions = map.into_transitions().collect::<Vec<_>>();
        statuses.sort();
        transitions.sort();
        assert_eq!(statuses, new_statuses);
        assert_eq!(transitions, new_transitions);
    }

    #[test]
    #[ignore = "slow"]
    fn random_inv_works() {
        let mut rng1 = rand::rng();
        let mut rng2 = rng1.clone();
        let states = Uniform::new(0u8, 100).unwrap();

        let mut map = <Map<_, _, RandomState>>::from_parts(
            (0u8..100).map(move |x| {
                (
                    x,
                    BiStatus::staying_leaving(
                        Status::accepting(rng1.random()),
                        Status::accepting(rng1.random()),
                    ),
                )
            }),
            iter::empty(),
        );

        let mut fwd = IndexSet::new();
        let mut bwd = IndexSet::new();

        for _ in 0..1000 {
            let start = rng2.sample(states);
            let input = rng2.sample(Alphanumeric);
            let end = rng2.sample(states);
            map.set_trans(start, input, end);
            verify_inv_matches(
                &mut fwd,
                &mut bwd,
                &map,
                format_args!("adding ({start:?}, {input:?}, {end:?})"),
            );
        }

        let map2 = map.clone();

        let mut rng3 = rng2.clone();
        let mut old_len = map.trans.len();
        while map.trans.len() > 1 {
            let lhs_idx = rng3.sample(Uniform::new(0, map.trans.len()).unwrap());
            let mut rhs_idx = rng3.sample(Uniform::new(0, map.trans.len() - 1).unwrap());
            if rhs_idx >= lhs_idx {
                rhs_idx += 1;
            }
            let lhs = *map.trans.get_index(lhs_idx).unwrap().0;
            let rhs = *map.trans.get_index(rhs_idx).unwrap().0;
            map.replace(lhs, rhs);
            verify_inv_matches(
                &mut fwd,
                &mut bwd,
                &map,
                format_args!("replacing {lhs:?} with {rhs:?}"),
            );
            let new_len = map.trans.len();
            assert!(new_len < old_len);
            old_len = new_len;
        }

        let mut map = map2;
        let mut old_len = map.trans.len();
        while map.trans.len() > 1 {
            let uniform = Uniform::new(0, map.trans.len()).unwrap();
            let lhs = *map.trans.get_index(rng3.sample(uniform)).unwrap().0;
            map.remove(lhs);
            verify_inv_matches(&mut fwd, &mut bwd, &map, format_args!("removing {lhs:?}"));
            let new_len = map.trans.len();
            assert!(new_len < old_len);
            old_len = new_len;
        }
    }
}
