//! [`Acyclic`] builder.

use core::ops::Deref;

use indexmap::IndexSet;

use crate::{
    auto::{Automaton, AutomatonBuilder, AutomatonMut, Map},
    hash::BuildHasher,
    scalars::{BiStatus, Input, ModeStatus, State},
};

/// Builder for acyclic automata with compression.
///
/// Incrementally creates a minimal "dictionary" automaton for recognizing a finite set of strings.
/// The result can be used to create an efficient automaton.
#[derive(Clone, Debug)]
pub struct Acyclic<
    S,
    I,
    #[cfg(feature = "std")] H = std::hash::RandomState,
    #[cfg(not(feature = "std"))] H,
> {
    /// Inner automaton.
    map: Map<S, I, H>,

    /// Register for states that need to be checked for equivalence.
    register: IndexSet<S, H>,
}

impl<S: State, I: Input, H: BuildHasher> Default for Acyclic<S, I, H> {
    #[coverage(off)]
    fn default() -> Self {
        Self::new()
    }
}

impl<S: State, I: Input, H: BuildHasher> Acyclic<S, I, H> {
    /// Creates an empty acyclic automaton.
    #[coverage(off)]
    pub fn new() -> Acyclic<S, I, H> {
        Acyclic {
            map: Map::new(BiStatus::RejectAll),
            register: IndexSet::with_hasher(H::default()),
        }
    }

    /// Adds a sequence to the automaton.
    pub fn insert(&mut self, word: &[I], how: ModeStatus) {
        let (last_state, prefix_len) = self.map.trans_slice(S::STARTING, word);
        let current_suffix = &word[prefix_len..];
        if let Some((_, child)) = self.map.last_trans(last_state) {
            self.replace_or_register(child);
        }
        self.add_suffix(last_state, current_suffix, how);
    }

    /// Replaces or registers the last state.
    fn replace_or_register(&mut self, child: S) {
        if let Some((_, last_child)) = self.map.last_trans(child) {
            self.replace_or_register(last_child);
        }
        for check in self.register.iter().copied() {
            if self.equivalent(check, child) {
                self.map.replace(check, child);
                if child > check {
                    self.map.rename(child, check);
                    self.register.swap_remove(&child);
                } else {
                    self.register.swap_remove(&check);
                    self.register.insert(child);
                }
                return;
            }
        }
        self.register.insert(child);
    }

    /// Checks whether two states are equivalent.
    fn equivalent(&self, lhs: S, rhs: S) -> bool {
        if self.map.status(lhs) != self.map.status(rhs) {
            return false;
        }
        if self.map.transes(lhs).len() != self.map.transes(rhs).len() {
            return false;
        }

        for (_, (ltrans, rtrans)) in self.map.zip_transes(lhs, rhs) {
            if ltrans != rtrans {
                return false;
            }
        }

        true
    }

    /// Adds a suffix to the current state.
    fn add_suffix(&mut self, mut last_state: S, current_suffix: &[I], how: ModeStatus) {
        let status = self.map.status(last_state);
        for input in current_suffix {
            let new_state = self.map.statuses().next_back().unwrap().0.bump();
            self.map.set_status(new_state, status);
            self.map.set_trans(last_state, *input, new_state);
            last_state = new_state;
        }
        self.map.set_status(
            last_state,
            match how {
                ModeStatus::RejectExact => BiStatus::RejectEmpty,
                ModeStatus::AcceptExact => BiStatus::AcceptEmpty,
                ModeStatus::RejectPrefix => BiStatus::RejectAll,
                ModeStatus::AcceptPrefix => BiStatus::AcceptAll,
            },
        );
    }

    /// Finish and return map.
    pub fn finish(mut self) -> Map<S, I, H> {
        if let Some((_, child)) = self.map.last_trans(S::STARTING) {
            self.replace_or_register(child);
        }
        self.map
    }
}
impl<S: State, I: Input, H> Deref for Acyclic<S, I, H> {
    type Target = Map<S, I, H>;

    #[inline]
    #[coverage(off)]
    fn deref(&self) -> &Self::Target {
        &self.map
    }
}
