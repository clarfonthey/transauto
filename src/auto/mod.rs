//! [`Automaton`] trait and friends.
#[cfg(feature = "alloc")]
use alloc::boxed::Box;
use core::{fmt, iter::Iterator as CoreIterator, ops::ControlFlow};

use crate::{
    iter::Iterator,
    scalars::{BiStatus, Input, State},
};

#[cfg(feature = "alloc")]
mod acyclic;
#[cfg(feature = "alloc")]
pub mod map;
#[cfg(feature = "alloc")]
pub use self::{acyclic::Acyclic, map::Map};

/// Common implementation for [`trans_slice`] and [`trans_iter`].
///
/// [`trans_slice`]: Automaton::trans_slice
/// [`trans_iter`]: Automaton::trans_iter
fn trans_impl<S: State, F: FnMut(S, I::Item) -> Option<S>, I: IntoIterator>(
    mut next: F,
    state: S,
    inputs: I,
) -> (S, usize) {
    match inputs
        .into_iter()
        .try_fold((state, 0), |(state, idx), input| match next(state, input) {
            Some(state) => ControlFlow::Continue((state, idx + 1)),
            None => ControlFlow::Break((state, idx)),
        }) {
        ControlFlow::Break((state, idx)) => (state, idx),
        ControlFlow::Continue((state, idx)) => (state, idx),
    }
}

/// Iterator that zips together the transitions for two states.
///
/// Will only include transitions that exist on both states.
///
/// FIXME: Return type notation.
pub struct ZipTranses<'a, A: ?Sized + Automaton, T> {
    /// [`transes`] iterator for left-hand-side state.
    ///
    /// [`transes`]: Automaton::transes
    ltranses: T,

    /// Right-hand-side state.
    rstate: A::State,

    /// Automaton.
    auto: &'a A,
}
impl<A: ?Sized + Automaton, T: Iterator<Item = (A::Input, A::State)>> CoreIterator
    for ZipTranses<'_, A, T>
{
    type Item = (A::Input, (A::State, A::State));

    fn next(&mut self) -> Option<Self::Item> {
        self.ltranses
            .by_ref()
            .filter_map(|(input, ltrans)| {
                self.auto
                    .trans(self.rstate, input)
                    .map(|rtrans| (input, (ltrans, rtrans)))
            })
            .next()
    }
}
impl<A: ?Sized + Automaton, T> fmt::Debug for ZipTranses<'_, A, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ZipTranses").finish_non_exhaustive()
    }
}

/// [Finite-state automaton].
///
/// It's designed to be as flexible as possible while still being possible to optimize.
///
/// [Finite-state automaton]: https://en.wikipedia.org/wiki/Deterministic_finite_automaton
pub trait Automaton {
    /// State type.
    ///
    /// State types may be able to represent more states than those included for any given
    /// automaton. [`statuses`] can be used to obtain a full list of states.
    ///
    /// In all cases, [`State::STARTING`] must not be considered out of bounds.
    ///
    /// [`statuses`]: Self::statuses
    type State: State;

    /// Input alphabet.
    ///
    /// Automata transition between states by accepting an input symbol with [`trans`].
    ///
    /// [`trans`]: Self::trans
    type Input: Input;

    /// Gets the status of a state.
    ///
    /// # Panics
    ///
    /// Panics if the state isn't in the automaton.
    fn status(&self, state: Self::State) -> BiStatus;

    /// Gets the statuses for all states in the automaton.
    ///
    /// This also doubles as a way to determine which states are "in bounds" for the automaton.
    fn statuses(&self) -> impl Iterator<Item = (Self::State, BiStatus)>;

    /// Gets the state after an input.
    ///
    /// If there is no state after an input, the resulting [`Status`] is determined by the
    /// [`leave`] [`status`] of the current state.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the automaton, although
    /// it may also return `None` instead.
    ///
    /// [`leave`]: BiStatus::leave
    /// [`status`]: Self::status
    fn trans(&self, state: Self::State, input: Self::Input) -> Option<Self::State>;

    /// Gets the lexicographically first [`trans`]ition for a state.
    ///
    /// Can be made more efficient depending on the implementation, but defaults to searching
    /// through all [`transes`].
    ///
    /// [`trans`]: Self::trans
    /// [`transes`]: Self::transes
    fn first_trans(&self, state: Self::State) -> Option<(Self::Input, Self::State)> {
        self.transes(state).min_by_key(|(input, _)| *input)
    }

    /// Gets the lexicographically last [`trans`]ition for a state.
    ///
    /// Can be made more efficient depending on the implementation, but defaults to searching
    /// through all [`transes`].
    ///
    /// [`trans`]: Self::trans
    /// [`transes`]: Self::transes
    fn last_trans(&self, state: Self::State) -> Option<(Self::Input, Self::State)> {
        self.transes(state).max_by_key(|(input, _)| *input)
    }

    /// Iterates over the possible [`trans`]es from a state.
    ///
    /// Yields input-state pairs, such that calling `next` on the initial state and the input
    /// yields the state for the pair.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the automaton, although
    /// it may also return an empty iterator instead.
    ///
    /// [`trans`]: Self::trans
    fn transes(&self, state: Self::State) -> impl Iterator<Item = (Self::Input, Self::State)>;

    /// Iterates over all [`trans`]itions in the automaton.
    ///
    /// Yields state-input-state triples, such that calling [`trans`] on the first state and input
    /// yields the second state.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the automaton, although
    /// it may also return an empty iterator instead.
    ///
    /// [`trans`]: Self::trans
    fn transitions(&self) -> impl Iterator<Item = (Self::State, Self::Input, Self::State)>;

    /// Performs the result of a [`trans`] for all inputs in a slice.
    ///
    /// This will stop when either the inputs are exhausted, or a transition has no resulting state.
    ///
    /// Even though this method defaults to a simple implementation, it can be better optimized than
    /// [`trans`] in some cases.
    ///
    /// # Panics
    ///
    /// This method inherits the panicking behavior from [`trans`].
    ///
    /// [`trans`]: Self::trans
    fn trans_slice(&self, state: Self::State, inputs: &[Self::Input]) -> (Self::State, usize) {
        trans_impl(
            |state, input| self.trans(state, input),
            state,
            inputs.iter().copied(),
        )
    }

    /// Performs the result of a [`trans`] for all inputs in an iterator.
    ///
    /// This will stop when either the inputs are exhausted, or a transition has no resulting state.
    ///
    /// Even though this method defaults to a simple implementation, it can be better optimized than
    /// [`trans`] in some cases.
    ///
    /// # Panics
    ///
    /// This method inherits the panicking behavior from [`trans`].
    ///
    /// [`trans`]: Self::trans
    fn trans_iter<Iter: IntoIterator<Item = Self::Input>>(
        &self,
        state: Self::State,
        inputs: Iter,
    ) -> (Self::State, usize)
    where
        Self: Sized,
    {
        trans_impl(|state, input| self.trans(state, input), state, inputs)
    }

    /// Zips together the [`transes`] for two states.
    ///
    /// Useful for checking the equivalence between two states. Excludes inputs that are only
    /// present on one state or the other; you have to check this separately.
    ///
    /// [`transes`]: Self::transes
    fn zip_transes(
        &self,
        lhs: Self::State,
        rhs: Self::State,
    ) -> ZipTranses<'_, Self, impl Iterator<Item = (Self::Input, Self::State)>>
    where
        Self: Sized,
    {
        ZipTranses {
            ltranses: self.transes(lhs),
            rstate: rhs,
            auto: self,
        }
    }
}
impl<'a, T: 'a + ?Sized + Automaton> Automaton for &'a T {
    type State = T::State;
    type Input = T::Input;

    #[inline]
    fn status(&self, state: T::State) -> BiStatus {
        (**self).status(state)
    }

    #[inline]
    fn statuses(&self) -> impl Iterator<Item = (T::State, BiStatus)> {
        (**self).statuses()
    }

    #[inline]
    fn trans(&self, state: T::State, input: T::Input) -> Option<T::State> {
        (**self).trans(state, input)
    }

    #[inline]
    fn transes(&self, state: T::State) -> impl Iterator<Item = (T::Input, T::State)> {
        (**self).transes(state)
    }

    #[inline]
    fn transitions(&self) -> impl Iterator<Item = (T::State, T::Input, T::State)> {
        (**self).transitions()
    }

    #[inline]
    fn trans_slice(&self, state: T::State, inputs: &[T::Input]) -> (T::State, usize) {
        (**self).trans_slice(state, inputs)
    }
}
impl<'a, T: 'a + ?Sized + Automaton> Automaton for &'a mut T {
    type State = T::State;
    type Input = T::Input;

    #[inline]
    fn status(&self, state: T::State) -> BiStatus {
        (**self).status(state)
    }

    #[inline]
    fn statuses(&self) -> impl Iterator<Item = (T::State, BiStatus)> {
        (**self).statuses()
    }

    #[inline]
    fn trans(&self, state: T::State, input: T::Input) -> Option<T::State> {
        (**self).trans(state, input)
    }

    #[inline]
    fn transes(&self, state: T::State) -> impl Iterator<Item = (T::Input, T::State)> {
        (**self).transes(state)
    }

    #[inline]
    fn transitions(&self) -> impl Iterator<Item = (T::State, T::Input, T::State)> {
        (**self).transitions()
    }

    #[inline]
    fn trans_slice(&self, state: T::State, inputs: &[T::Input]) -> (T::State, usize) {
        (**self).trans_slice(state, inputs)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + Automaton> Automaton for Box<T> {
    type State = T::State;
    type Input = T::Input;

    #[inline]
    fn status(&self, state: T::State) -> BiStatus {
        (**self).status(state)
    }

    #[inline]
    fn statuses(&self) -> impl Iterator<Item = (T::State, BiStatus)> {
        (**self).statuses()
    }

    #[inline]
    fn trans(&self, state: T::State, input: T::Input) -> Option<T::State> {
        (**self).trans(state, input)
    }

    #[inline]
    fn transes(&self, state: T::State) -> impl Iterator<Item = (T::Input, T::State)> {
        (**self).transes(state)
    }

    #[inline]
    fn transitions(&self) -> impl Iterator<Item = (T::State, T::Input, T::State)> {
        (**self).transitions()
    }

    #[inline]
    fn trans_slice(&self, state: T::State, inputs: &[T::Input]) -> (T::State, usize) {
        (**self).trans_slice(state, inputs)
    }
}

/// An [`Automaton`] that goes both ways.
pub trait BiAutomaton: Automaton {
    /// Gets the states before an input.
    ///
    /// This returns the states which, if called with the given input, would yield the provided
    /// starting state.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the automaton or the
    /// starting state, although it may also return an empty iterator instead.
    fn cis(&self, input: Self::Input, state: Self::State) -> impl Iterator<Item = Self::State>;

    /// Iterates over the possible [`cis`]es from a state.
    ///
    /// Yields state-input pairs, such that calling `next` on the state and the input
    /// yields the initial state.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the automaton or the
    /// starting state, although it may also return an empty iterator instead.
    ///
    /// [`cis`]: Self::cis
    fn cises(&self, state: Self::State) -> impl Iterator<Item = (Self::State, Self::Input)>;
}
impl<'a, T: 'a + ?Sized + BiAutomaton> BiAutomaton for &'a T {
    #[inline]
    fn cis(&self, input: T::Input, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).cis(input, state)
    }

    #[inline]
    fn cises(&self, state: T::State) -> impl Iterator<Item = (T::State, T::Input)> {
        (**self).cises(state)
    }
}
impl<'a, T: 'a + ?Sized + BiAutomaton> BiAutomaton for &'a mut T {
    #[inline]
    fn cis(&self, input: T::Input, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).cis(input, state)
    }

    #[inline]
    fn cises(&self, state: T::State) -> impl Iterator<Item = (T::State, T::Input)> {
        (**self).cises(state)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + BiAutomaton> BiAutomaton for Box<T> {
    #[inline]
    fn cis(&self, input: T::Input, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).cis(input, state)
    }

    #[inline]
    fn cises(&self, state: T::State) -> impl Iterator<Item = (T::State, T::Input)> {
        (**self).cises(state)
    }
}

/// Mutable [`Automaton`]
///
/// Generally, these are builders for automata which aren't ready to run. Once the automata are
/// complete, they can be optimized for proper running.
pub trait AutomatonMut: Automaton {
    /// Creates a state with the given status, or sets the status of an existing state.
    fn set_status(&mut self, state: Self::State, status: BiStatus) -> Option<BiStatus>;

    /// Sets the state after an input, returning the old transition.
    ///
    /// # Panics
    ///
    /// This method panics if given a state which is not included in the automaton.
    fn set_trans(
        &mut self,
        state: Self::State,
        input: Self::Input,
        next: Self::State,
    ) -> Option<Self::State>;

    /// Deletes a state from the automaton, if it exists.
    ///
    /// May end up renaming existing states to avoid gaps in IDs.
    ///
    /// # Panics
    ///
    /// Panics on removing the start state.
    fn remove(&mut self, state: Self::State) -> bool;

    /// Replaces all occurrences of a state with another, deleting it.
    ///
    /// # Panics
    ///
    /// Panics if replacing the start state, or if the destination state does not exist.
    fn replace(&mut self, src: Self::State, dest: Self::State);

    /// Renames all occurrences of a state to a different ID.
    ///
    /// # Panics
    ///
    /// Panics if renaming the start state, or if the destination state exists.
    fn rename(&mut self, src: Self::State, dest: Self::State);
}
impl<'a, T: 'a + ?Sized + AutomatonMut> AutomatonMut for &'a mut T {
    #[inline]
    fn set_status(&mut self, state: T::State, status: BiStatus) -> Option<BiStatus> {
        (**self).set_status(state, status)
    }

    #[inline]
    fn set_trans(&mut self, state: T::State, input: T::Input, next: T::State) -> Option<T::State> {
        (**self).set_trans(state, input, next)
    }

    #[inline]
    fn remove(&mut self, state: T::State) -> bool {
        (**self).remove(state)
    }

    #[inline]
    fn replace(&mut self, src: T::State, dest: T::State) {
        (**self).replace(src, dest)
    }

    #[inline]
    fn rename(&mut self, src: T::State, dest: T::State) {
        (**self).rename(src, dest)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + AutomatonMut> AutomatonMut for Box<T> {
    #[inline]
    fn set_status(&mut self, state: T::State, status: BiStatus) -> Option<BiStatus> {
        (**self).set_status(state, status)
    }

    #[inline]
    fn set_trans(&mut self, state: T::State, input: T::Input, next: T::State) -> Option<T::State> {
        (**self).set_trans(state, input, next)
    }

    #[inline]
    fn remove(&mut self, state: T::State) -> bool {
        (**self).remove(state)
    }

    #[inline]
    fn replace(&mut self, src: T::State, dest: T::State) {
        (**self).replace(src, dest)
    }

    #[inline]
    fn rename(&mut self, src: T::State, dest: T::State) {
        (**self).rename(src, dest)
    }
}

/// [`Automaton`] builder.
///
/// Non-object-safe extension to [`AutomatonMut`].
pub trait AutomatonBuilder: Sized + AutomatonMut {
    /// Creates a new automaton, given the status of the starting state.
    fn new(status: BiStatus) -> Self;

    /// Creates a new automaton from a series of statuses and transitions.
    ///
    /// # Panics
    ///
    /// Panics if the starting state is never initialized.
    fn from_parts(
        statuses: impl IntoIterator<Item = (Self::State, BiStatus)>,
        transitions: impl IntoIterator<Item = (Self::State, Self::Input, Self::State)>,
    ) -> Self {
        let mut new = Self::new(BiStatus::RejectAll);
        let mut set_start = false;
        for (state, status) in statuses {
            new.set_status(state, status);
            if state == Self::State::STARTING {
                set_start = true;
            }
        }
        assert!(set_start, "didn't provide status for start state");
        for (state, input, next) in transitions {
            new.set_trans(state, input, next);
        }
        new
    }

    /// Splits an automaton into just its statuses.
    fn into_statuses(self) -> impl Iterator<Item = (Self::State, BiStatus)>;

    /// Splits an automaton into just its transitions.
    fn into_transitions(self) -> impl Iterator<Item = (Self::State, Self::Input, Self::State)>;

    /// Splits an automaton into its parts.
    fn into_parts(
        self,
    ) -> impl Iterator<
        Item = (
            Self::State,
            BiStatus,
            impl Iterator<Item = (Self::Input, Self::State)>,
        ),
    >;
}
#[cfg(feature = "alloc")]
impl<T: AutomatonBuilder> AutomatonBuilder for Box<T> {
    #[inline]
    fn new(status: BiStatus) -> Box<T> {
        Box::new(T::new(status))
    }

    #[inline]
    fn from_parts(
        statuses: impl IntoIterator<Item = (T::State, BiStatus)>,
        transitions: impl IntoIterator<Item = (T::State, T::Input, T::State)>,
    ) -> Box<T> {
        Box::new(T::from_parts(statuses, transitions))
    }

    #[inline]
    fn into_statuses(self) -> impl Iterator<Item = (T::State, BiStatus)> {
        (*self).into_statuses()
    }

    #[inline]
    fn into_transitions(self) -> impl Iterator<Item = (T::State, T::Input, T::State)> {
        (*self).into_transitions()
    }
    #[inline]
    fn into_parts(
        self,
    ) -> impl Iterator<
        Item = (
            T::State,
            BiStatus,
            impl Iterator<Item = (T::Input, T::State)>,
        ),
    > {
        (*self).into_parts()
    }
}

/// [Non-deterministic automaton].
///
/// It's designed to be as flexible as possible while still being possible to optimize.
///
/// [non-deterministic automaton]: https://en.wikipedia.org/wiki/Nondeterministic_finite_automaton
pub trait MultiAutomaton: Automaton {
    /// Gets the states that are automatically reachable from a state, if any.
    ///
    /// These are the non-deterministic states for non-deterministic automata. They're also called
    /// ε-states or ε-transitions.
    ///
    /// The result is expected to be pre-closed, i.e., calling [`close`] on the yielded states
    /// will not yield more states than those yielded by this iterator.
    ///
    /// # Panics
    ///
    /// This method *may* panic if a given a state which is not included in the automaton, although
    /// it may also return [`None`] instead.
    ///
    /// [`close`]: Self::close
    fn close(&self, state: Self::State) -> impl Iterator<Item = Self::State>;
}
impl<'a, T: 'a + ?Sized + MultiAutomaton> MultiAutomaton for &'a T {
    #[inline]
    fn close(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).close(state)
    }
}
impl<'a, T: 'a + ?Sized + MultiAutomaton> MultiAutomaton for &'a mut T {
    #[inline]
    fn close(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).close(state)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + MultiAutomaton> MultiAutomaton for Box<T> {
    #[inline]
    fn close(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).close(state)
    }
}

/// A [`MultiAutomaton`] that goes both ways.
pub trait BiMultiAutomaton: MultiAutomaton + BiAutomaton {
    /// Gets the states that can automatically reach a state, if any.
    ///
    /// Expected to be pre-closed, i.e., calling [`open`] on the yielded states will not yield
    /// more states than those yielded by this iterator.
    ///
    /// # Panics
    ///
    /// This method *may* panic if a given a state which is not included in the automaton, although
    /// it may also return [`None`] instead.
    ///
    /// [`open`]: Self::open
    fn open(&self, state: Self::State) -> impl Iterator<Item = Self::State>;
}
impl<'a, T: 'a + ?Sized + BiMultiAutomaton> BiMultiAutomaton for &'a T {
    #[inline]
    fn open(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).open(state)
    }
}
impl<'a, T: 'a + ?Sized + BiMultiAutomaton> BiMultiAutomaton for &'a mut T {
    #[inline]
    fn open(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).open(state)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + BiMultiAutomaton> BiMultiAutomaton for Box<T> {
    #[inline]
    fn open(&self, state: T::State) -> impl Iterator<Item = T::State> {
        (**self).open(state)
    }
}
