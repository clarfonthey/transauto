//! [`Transducer`] trait and friends.
#[cfg(feature = "alloc")]
use alloc::boxed::Box;

use crate::{
    auto::{Automaton, MultiAutomaton},
    iter::Iterator,
    scalars::Output,
};

/// Finite-state transducer.
pub trait Transducer: Automaton {
    /// Output alphabet.
    ///
    /// When transitioning between states, transducers can output zero or more output symbols.
    ///
    /// Depending on the transducer, these output symbols are intended to be folded together
    /// (for example, in the case of [Levenstein transducers]) or concatenated into a larger string
    /// (for example, in the case of [morphological dictionaries]).
    ///
    /// This trait does not distinguish between folding and concatenating outputs, instead always
    /// offering outputs as an iterator that can be optimised for one method or the other.
    ///
    /// [Levenstein transducers]: https://en.wikipedia.org/wiki/Levenshtein_transducer
    /// [morphological dictionaries]: https://en.wikipedia.org/wiki/Morphological_dictionary
    type Output: Output;

    /// Gets the output after an input.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the transducer, although
    /// it may also return an empty iterator instead.
    fn form(&self, state: Self::State, input: Self::Input) -> impl Iterator<Item = Self::Output>;

    /// Iterates over the possible [`form`]s from a state.
    ///
    /// # Panics
    ///
    /// This method *may* panic if given a state which is not included in the transducer, although
    /// it may also return an empty iterator instead.
    ///
    /// [`form`]: Self::form
    fn forms(
        &self,
        state: Self::State,
    ) -> impl Iterator<Item = (Self::Input, impl Iterator<Item = Self::Output>)>;

    /// Iterates over all trans[`form`]s in the transducer.
    ///
    /// [`form`]: Self::form
    fn transforms(
        &self,
    ) -> impl Iterator<Item = (Self::State, Self::Input, impl Iterator<Item = Self::Output>)>;
}
impl<'a, T: 'a + ?Sized + Transducer> Transducer for &'a T {
    type Output = T::Output;

    #[inline]
    #[coverage(off)]
    fn form(&self, state: T::State, input: T::Input) -> impl Iterator<Item = T::Output> {
        (**self).form(state, input)
    }

    #[inline]
    #[coverage(off)]
    fn forms(
        &self,
        state: T::State,
    ) -> impl Iterator<Item = (T::Input, impl Iterator<Item = T::Output>)> {
        (**self).forms(state)
    }

    #[inline]
    #[coverage(off)]
    fn transforms(
        &self,
    ) -> impl Iterator<Item = (T::State, T::Input, impl Iterator<Item = T::Output>)> {
        (**self).transforms()
    }
}
impl<'a, T: 'a + ?Sized + Transducer> Transducer for &'a mut T {
    type Output = T::Output;

    #[inline]
    #[coverage(off)]
    fn form(&self, state: T::State, input: T::Input) -> impl Iterator<Item = T::Output> {
        (**self).form(state, input)
    }

    #[inline]
    #[coverage(off)]
    fn forms(
        &self,
        state: T::State,
    ) -> impl Iterator<Item = (T::Input, impl Iterator<Item = T::Output>)> {
        (**self).forms(state)
    }

    #[inline]
    #[coverage(off)]
    fn transforms(
        &self,
    ) -> impl Iterator<Item = (T::State, T::Input, impl Iterator<Item = T::Output>)> {
        (**self).transforms()
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + Transducer> Transducer for Box<T> {
    type Output = T::Output;

    #[inline]
    #[coverage(off)]
    fn form(&self, state: T::State, input: T::Input) -> impl Iterator<Item = T::Output> {
        (**self).form(state, input)
    }

    #[inline]
    #[coverage(off)]
    fn forms(
        &self,
        state: T::State,
    ) -> impl Iterator<Item = (T::Input, impl Iterator<Item = T::Output>)> {
        (**self).forms(state)
    }

    #[inline]
    #[coverage(off)]
    fn transforms(
        &self,
    ) -> impl Iterator<Item = (T::State, T::Input, impl Iterator<Item = T::Output>)> {
        (**self).transforms()
    }
}

/// Nondeterministic transducer.
pub trait MultiTransducer: Transducer + MultiAutomaton {
    /// Iterator over zero-input forms that are reachable from a state.
    ///
    /// Gets the outputs associated with a state transition.
    ///
    /// These are the non-deterministic outputs for non-deterministic transducers. They're also
    /// called ε-states or ε-transitions.
    ///
    /// # Panics
    ///
    /// This method *may* panic if a given a state which is not included in the automaton, although
    /// it may also return [`None`] instead.
    fn close_form(&self, src: Self::State, dest: Self::State)
    -> impl Iterator<Item = Self::Output>;

    /// Gets all outputs associated with a zero-input transition.
    ///
    /// These are the non-deterministic outputs for non-deterministic transducers. They're also
    /// called ε-states or ε-transitions.
    ///
    /// Expected to be pre-closed, i.e., calling [`close_forms`] on the yielded states will not
    /// yield more states than those yielded by this iterator.
    ///
    /// [`close_forms`]: Self::close_forms
    ///
    /// # Panics
    ///
    /// This method *may* panic if a given a state which is not included in the automaton, although
    /// it may also return [`None`] instead.
    fn close_forms(
        &self,
        src: Self::State,
    ) -> impl Iterator<Item = (Self::State, impl Iterator<Item = Self::Output>)>;
}
impl<'a, T: 'a + ?Sized + MultiTransducer> MultiTransducer for &'a T {
    #[inline]
    #[coverage(off)]
    fn close_form(&self, src: T::State, dest: T::State) -> impl Iterator<Item = T::Output> {
        (**self).close_form(src, dest)
    }

    #[inline]
    #[coverage(off)]
    fn close_forms(
        &self,
        src: T::State,
    ) -> impl Iterator<Item = (T::State, impl Iterator<Item = T::Output>)> {
        (**self).close_forms(src)
    }
}
impl<'a, T: 'a + ?Sized + MultiTransducer> MultiTransducer for &'a mut T {
    #[inline]
    #[coverage(off)]
    fn close_form(&self, src: T::State, dest: T::State) -> impl Iterator<Item = T::Output> {
        (**self).close_form(src, dest)
    }

    #[inline]
    #[coverage(off)]
    fn close_forms(
        &self,
        src: T::State,
    ) -> impl Iterator<Item = (T::State, impl Iterator<Item = T::Output>)> {
        (**self).close_forms(src)
    }
}
#[cfg(feature = "alloc")]
impl<T: ?Sized + MultiTransducer> MultiTransducer for Box<T> {
    #[inline]
    #[coverage(off)]
    fn close_form(&self, src: T::State, dest: T::State) -> impl Iterator<Item = T::Output> {
        (**self).close_form(src, dest)
    }

    #[inline]
    #[coverage(off)]
    fn close_forms(
        &self,
        src: T::State,
    ) -> impl Iterator<Item = (T::State, impl Iterator<Item = T::Output>)> {
        (**self).close_forms(src)
    }
}
